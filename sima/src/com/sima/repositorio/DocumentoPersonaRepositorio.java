package com.sima.repositorio;

import com.sima.entidad.DocumentoPersona;
import com.sima.entidad.DocumentoPersonaId;
import com.sima.entidad.Persona;

public interface DocumentoPersonaRepositorio extends BaseRepositorio<DocumentoPersona, DocumentoPersonaId>{
	public DocumentoPersona obtenerDocumentoByPersona(Persona persona);
}
