package com.sima.repositorio;

import java.math.BigDecimal;

public interface CuisTasaJDBCRepositorio {
	public BigDecimal getUITPorAnio(String anio);
}
