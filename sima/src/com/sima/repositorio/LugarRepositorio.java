package com.sima.repositorio;

import com.sima.entidad.Lugar;
import com.sima.entidad.Persona;

public interface LugarRepositorio extends BaseRepositorio<Lugar, String> {
	
	public Lugar obtenerLugarByPersona(Persona persona);

}
