package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.Grupo;
import com.sima.repositorio.GrupoRepositorio;

@Repository
public class GrupoRepositorioImpl extends BaseRepositorioImpl<Grupo, Long>
		implements GrupoRepositorio {

}
