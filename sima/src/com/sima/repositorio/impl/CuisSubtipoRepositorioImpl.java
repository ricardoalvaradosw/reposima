package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.CuisSubtipo;
import com.sima.repositorio.CuisSubtipoRepositorio;

@Repository
public class CuisSubtipoRepositorioImpl extends BaseRepositorioImpl<CuisSubtipo, Long> implements CuisSubtipoRepositorio{

}
