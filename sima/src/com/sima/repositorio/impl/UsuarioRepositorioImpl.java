package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.Usuario;
import com.sima.repositorio.UsuarioRepositorio;

@Repository
public class UsuarioRepositorioImpl extends
		BaseRepositorioImpl<Usuario, Long> implements UsuarioRepositorio {

}