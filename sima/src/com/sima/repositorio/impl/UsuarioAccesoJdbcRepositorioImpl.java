package com.sima.repositorio.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sima.repositorio.UsuarioAccesoJdbcRepositorio;

@Repository
public class UsuarioAccesoJdbcRepositorioImpl implements UsuarioAccesoJdbcRepositorio {
	
	@Autowired
	private DataSource dataSource;

	@Override
	public Long getCodigoGrupo(Long codigoUsuarioAcceso) {
		Long idUsuarioAcceso=null;
		try{
			Connection connection=dataSource.getConnection();
			Statement statement=connection.createStatement();
			ResultSet resultSet=statement.executeQuery("select codgrupo from \"NoTributario\".usuarioacceso where id="+codigoUsuarioAcceso);
			while(resultSet.next()){
//				idUsuarioAcceso=Long.parseLong(resultSet.getString("codgrupo"));
				String codgrupo = String.valueOf(resultSet.getInt(1));
				idUsuarioAcceso = Long.parseLong(codgrupo);
			}
			resultSet.close();
			statement.close();
			connection.close();
		}catch(Exception exception){
			exception.printStackTrace();
		}		
		return idUsuarioAcceso;
	}

}
