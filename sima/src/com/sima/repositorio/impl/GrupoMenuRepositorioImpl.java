package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.GrupoMenu;
import com.sima.repositorio.GrupoMenuRepositorio;

@Repository
public class GrupoMenuRepositorioImpl extends
		BaseRepositorioImpl<GrupoMenu, Long> implements
		GrupoMenuRepositorio {

}
