package com.sima.repositorio.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projection;
import org.springframework.stereotype.Repository;

import com.sima.entidad.Persona;
import com.sima.repositorio.PersonaRepositorio;
import com.sima.util.Criterio;
import com.sima.util.Queries;


@Repository
public class PersonaRepositorioImpl extends BaseRepositorioImpl<Persona, String> implements PersonaRepositorio {

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> findByCriteria(Criterio criterio) {
		Criteria busqueda = criterio.getExecutableCriteria(this.sessionFactory
				.getCurrentSession());
		busqueda.setProjection(null);
		//busqueda.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		busqueda.setFirstResult(((Criterio) criterio).getFirstResult());
		return (List<Persona>) busqueda.list();
	}

	@Override
	public Map<Object,Object> getQueryForSearch(Persona persona) {
		// TODO Auto-generated method stub
		Map<Object,Object> result = new HashMap<>();
		Query query = this.sessionFactory.getCurrentSession().createQuery(Queries.PERSON_QUERY_SEARCH);
		query.setString(0, '%' + persona.getNombre() + '%');
		query.setString(1, '%' + persona.getDocumentosIdentidad().get(0).getNumero() + '%');
		
		Long size = ( (Long) this.sessionFactory.getCurrentSession().createQuery(Queries.PERSON_QUERY_COUNT).setString(0,'%' + persona.getNombre() + '%').setString(1, '%' + persona.getDocumentosIdentidad().get(0).getNumero() + '%').iterate().next() ).longValue();
		
		result.put("query", query);
		result.put("size", size);
		
		return result;
	}

	

}
