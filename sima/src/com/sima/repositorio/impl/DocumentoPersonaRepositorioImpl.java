package com.sima.repositorio.impl;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.sima.entidad.DocumentoPersona;
import com.sima.entidad.DocumentoPersonaId;
import com.sima.entidad.Persona;
import com.sima.repositorio.DocumentoPersonaRepositorio;

@Repository
public class DocumentoPersonaRepositorioImpl extends BaseRepositorioImpl<DocumentoPersona, DocumentoPersonaId> implements DocumentoPersonaRepositorio {

	@Override
	public DocumentoPersona obtenerDocumentoByPersona(Persona persona) {
		// TODO Auto-generated method stub
		Criteria criteria = this.getCurrentSession().createCriteria(DocumentoPersona.class,"documentoPersona");
		criteria.setFetchMode("documentoPersona.persona", FetchMode.JOIN);
		criteria.createAlias("documentoPersona.persona", "persona");
		criteria.add(Restrictions.eq("persona.idPersona", persona.getIdPersona()));
		criteria.setProjection(Projections.projectionList()
				.add(Projections.property("numero"),"numero"));
		criteria.setResultTransformer(Transformers.aliasToBean(DocumentoPersona.class));
		criteria.setMaxResults(1);
		
		return (DocumentoPersona)criteria.uniqueResult();
	}
	
}
