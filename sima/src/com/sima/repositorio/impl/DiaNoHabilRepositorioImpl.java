package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.DiaNoHabil;
import com.sima.repositorio.DiaNoHabilRepositorio;
@Repository
public class DiaNoHabilRepositorioImpl extends BaseRepositorioImpl<DiaNoHabil, Long>
	implements DiaNoHabilRepositorio
{

}
