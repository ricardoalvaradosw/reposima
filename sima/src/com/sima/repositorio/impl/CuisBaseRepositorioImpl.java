package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.CuisBase;
import com.sima.repositorio.CuisBaseRepositorio;

@Repository
public class CuisBaseRepositorioImpl extends BaseRepositorioImpl<CuisBase, Long> implements CuisBaseRepositorio {

}
