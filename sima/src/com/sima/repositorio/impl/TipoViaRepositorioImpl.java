package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.TipoVia;
import com.sima.repositorio.TipoViaRepositorio;

@Repository
public class TipoViaRepositorioImpl extends BaseRepositorioImpl<TipoVia, Short> implements TipoViaRepositorio {

}
