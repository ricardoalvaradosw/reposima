package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.Menu;
import com.sima.repositorio.MenuRepositorio;

@Repository
public class MenuRepositorioImpl extends BaseRepositorioImpl<Menu, Long> implements MenuRepositorio{
	
}
