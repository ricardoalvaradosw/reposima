package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.UsuarioAcceso;
import com.sima.repositorio.UsuarioAccesoRepositorio;

@Repository
public class UsuarioAccesoRepositorioImpl extends
		BaseRepositorioImpl<UsuarioAcceso, Long> implements
		UsuarioAccesoRepositorio {

}
