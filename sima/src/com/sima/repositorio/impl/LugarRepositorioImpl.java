package com.sima.repositorio.impl;


import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.sima.entidad.Lugar;
import com.sima.entidad.Persona;
import com.sima.repositorio.LugarRepositorio;

@Repository
public class LugarRepositorioImpl extends BaseRepositorioImpl<Lugar, String> implements LugarRepositorio{

	@Override
	public Lugar obtenerLugarByPersona(Persona persona) {
		// TODO Auto-generated method stub
		Criteria criteria = this.getCurrentSession().createCriteria(Lugar.class,"lugar");
		criteria.setFetchMode("lugar.personas",FetchMode.JOIN);
		criteria.createAlias("lugar.personas", "persona");
		criteria.add(Restrictions.eq("persona.idPersona", persona.getIdPersona()));
		criteria.setProjection(Projections.projectionList()
				.add(Projections.property("nombre"),"nombre"));
		criteria.setResultTransformer(Transformers.aliasToBean(Lugar.class));
		return (Lugar)criteria.uniqueResult();
	}

}
