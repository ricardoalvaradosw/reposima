package com.sima.repositorio.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sima.repositorio.CuisTasaJDBCRepositorio;

@Repository
public class CuisTasaJDBCRepositorioImpl implements CuisTasaJDBCRepositorio {
	
	@Autowired
	private DataSource dataSource;
	

	@Override
	public BigDecimal getUITPorAnio(String anio) {
		// TODO Auto-generated method stub
		BigDecimal valorUIT = null;
		Connection conection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conection = dataSource.getConnection();
			ps = conection.prepareStatement("select n0405valor1 from \"t0405Parametros\" where c0405anio = ? and c0405descripcion = ?");
			
			ps.setString(1, anio);
			ps.setString(2, "UIT");
			rs = ps.executeQuery();
			while(rs.next()){
				valorUIT = rs.getBigDecimal(1);
			}
			rs.close();
			ps.close();
			conection.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return valorUIT;
	}

}
