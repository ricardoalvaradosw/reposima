package com.sima.repositorio.impl;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.sima.entidad.Lugar;
import com.sima.entidad.TipoLugar;
import com.sima.repositorio.TipoLugarRepositorio;

@Repository
public class TipoLugarRepositorioImpl extends BaseRepositorioImpl<TipoLugar,Integer> implements TipoLugarRepositorio {

	@Override
	public TipoLugar obtenerTipoLugarByLugar(Lugar lugar) {
		// TODO Auto-generated method stub
		Criteria criteria = this.getCurrentSession().createCriteria(TipoLugar.class,"tipoLugar");
		criteria.setFetchMode("tipoLugar.lugares", FetchMode.JOIN);
		criteria.createAlias("tipoLugar.lugares", "lugar");
		criteria.add(Restrictions.eq("lugar.id", lugar.getId()));
		return (TipoLugar)criteria.uniqueResult();
	}

}
