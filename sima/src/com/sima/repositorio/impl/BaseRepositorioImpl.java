package com.sima.repositorio.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTransactionManager;

import com.sima.repositorio.BaseRepositorio;
import com.sima.util.Criterio;

public class BaseRepositorioImpl<Entidad extends Serializable, TipoLlave extends Serializable>
		implements BaseRepositorio<Entidad, TipoLlave> {

	protected static final Log logger = LogFactory
			.getLog(HibernateTransactionManager.class);

	@Autowired
	protected SessionFactory sessionFactory;

	protected Class<Entidad> domainClass;

	@SuppressWarnings("unchecked")
	public BaseRepositorioImpl() {
		super();
		this.domainClass = (Class<Entidad>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public Entidad obtener(TipoLlave id) {
		return (Entidad) this.sessionFactory.getCurrentSession().get(
				domainClass, id);
	}

	public void actualizar(Entidad object) {
		getCurrentSession().saveOrUpdate(object);
	}

	public void crear(Entidad object) {
		getCurrentSession().save(object);
	}

	public void grabarTodos(List<Entidad> list) {
		for (Entidad entidad : list) {
			this.sessionFactory.getCurrentSession().save(entidad);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Entidad> buscarPorCriteria(Criterio filtro) {
		Criteria busqueda = filtro.getExecutableCriteria(this.sessionFactory
				.getCurrentSession());
		busqueda.setProjection(null);
		busqueda.setFirstResult(((Criterio) filtro).getFirstResult());
		return (List<Entidad>) busqueda.list();
	}

	@SuppressWarnings("unchecked")
	public Long cantidadPorCriteria(Criterio filtro) {
		Criteria busqueda = filtro.getExecutableCriteria(this.sessionFactory
				.getCurrentSession());
		busqueda.setFirstResult(((Criterio) filtro).getFirstResult());
		busqueda.setProjection(Projections.rowCount());
		List<Long> a = (List<Long>) busqueda.list();
		return a.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Entidad> obtenerTodos() {
		Criterio filtro = Criterio.forClass(domainClass);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		Criteria busqueda = filtro.getExecutableCriteria(this.sessionFactory
				.getCurrentSession());
		busqueda.setProjection(null);
		busqueda.setFirstResult(((Criterio) filtro).getFirstResult());
		return (List<Entidad>) busqueda.list();
	}
	

}