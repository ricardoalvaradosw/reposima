package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.CuisDetalle;
import com.sima.repositorio.CuisDetalleRepositorio;

@Repository
public class CuisDetalleRepositorioImpl extends
		BaseRepositorioImpl<CuisDetalle, Long> implements
		CuisDetalleRepositorio {

}
