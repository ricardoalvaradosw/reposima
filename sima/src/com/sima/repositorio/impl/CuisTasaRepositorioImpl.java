package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.CuisTasa;
import com.sima.repositorio.CuisTasaRepositorio;

@Repository
public class CuisTasaRepositorioImpl extends
		BaseRepositorioImpl<CuisTasa, Long> implements CuisTasaRepositorio {

}
