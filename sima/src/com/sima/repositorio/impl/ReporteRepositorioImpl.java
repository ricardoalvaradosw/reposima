package com.sima.repositorio.impl;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sima.repositorio.ReporteRepositorio;


@Repository
public class ReporteRepositorioImpl implements ReporteRepositorio {
	
	@Autowired
	private DataSource dataSource;
	private Logger logger = Logger.getLogger(this.getClass());

	public JasperPrint generarReporte(String reportName,Map<String, Object> parameters) {
		logger.info("ReporteRepositorioImpl.generarReporte Start");
		logger.info("reportName="+reportName+",parameters="+parameters);
		JasperPrint response = null;
		JRBeanCollectionDataSource collectionDataSource=null;
        JasperReport jasperReport=null;
        try{
        	jasperReport=(JasperReport)JRLoader.loadObject(this.getClass().getResource("/com/sima/reportes/"+reportName+".jasper"));
        		Connection connection=dataSource.getConnection();
        		response=JasperFillManager.fillReport(jasperReport,parameters,connection);
        		connection.close();
            logger.info("response="+response); 
        }catch(Exception exception){
        	exception.printStackTrace();
        }
        logger.info("::: ReporteRepositorioImpl.generarReporte End :::");
		return response;
	}

	
}