package com.sima.repositorio.impl;

import org.springframework.stereotype.Repository;

import com.sima.entidad.Via;
import com.sima.repositorio.ViaRepositorio;
@Repository
public class ViaRepositorioImpl extends BaseRepositorioImpl<Via,String> implements ViaRepositorio{

}
