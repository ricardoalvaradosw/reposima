package com.sima.repositorio;

import com.sima.entidad.Lugar;
import com.sima.entidad.TipoLugar;

public interface TipoLugarRepositorio extends BaseRepositorio<TipoLugar, Integer> {

	public TipoLugar obtenerTipoLugarByLugar(Lugar lugar);
}
