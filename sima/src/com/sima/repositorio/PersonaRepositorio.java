package com.sima.repositorio;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import com.sima.entidad.Persona;
import com.sima.util.Criterio;


public interface PersonaRepositorio extends BaseRepositorio<Persona, String> {
	public List<Persona> findByCriteria(Criterio criterio);
	
	
	public Map<Object,Object> getQueryForSearch(Persona persona);
	
	
}
