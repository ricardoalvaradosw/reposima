package com.sima.repositorio;

import java.util.List;
import java.util.Map;


import net.sf.jasperreports.engine.JasperPrint;

public interface ReporteRepositorio {
	
	public JasperPrint generarReporte(String reportName,Map<String,Object> parameters);

}