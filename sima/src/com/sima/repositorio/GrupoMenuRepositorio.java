package com.sima.repositorio;

import com.sima.entidad.GrupoMenu;

public interface GrupoMenuRepositorio extends
		BaseRepositorio<GrupoMenu, Long> {

}
