package com.sima.repositorio;

import com.sima.entidad.Usuario;

public interface UsuarioRepositorio extends BaseRepositorio<Usuario, Long> {

}