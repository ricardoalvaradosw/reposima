package com.sima.util;

public class TextField extends com.vaadin.ui.TextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getValue(){
		return super.getValue().trim().toUpperCase();
	}
}
