package com.sima.util;

public class Queries {
	
	public final static String PERSON_QUERY_SEARCH = "select p from Persona as p inner join p.documentosIdentidad as doc where lower(p.nombre) like ? and doc.numero like ? order by p.nombre";
	public final static String PERSON_QUERY_COUNT = "select count(*) from Persona as p inner join p.documentosIdentidad as doc where lower(p.nombre) like ? and doc.numero like ?";
	
}
