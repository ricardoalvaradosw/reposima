package com.sima.util;

public class Constantes {
	public static String NOMBRE_EMPRESA = "";

	public static abstract class PAGINACION {
		public static Long USUARIO = 10L;
		public static Long SISTEMA = 10L;
		public static Long GRUPO = 10L;
		public static Long CUISSUBTIPO = 10L;
		public static Long CUISBASE = 10L;
		public static Long CUIS = 15L;
		public static Long PERSONA = 8L;

	}

	public static abstract class TIPO_OPERACION {
		public static Integer MODIFICACION = 1;
		public static Integer ELIMINACION = 2;
	}

	public static abstract class SESION {
		public final static String USUARIO = "SESION_USUARIO";
		public final static String USUARIO_CODIGO = "SESION_USUARIO_CODIGO";
	}

	public static abstract class OPCION {
		public final static String MANTENIMIENTO_PERMISOS = "frmPermisos";
		public final static String MANTENIMIENTO_GRUPOS = "frmGrupos";
		public final static String MANTENIMIENTO_TIPODOCUMENTO = "frmTipoDocumento";
		public final static String MANTENIMIENTO_PAP_ADM ="frmPapAdm";
		public final static String MANTENIMIENTO_PERSONAS = "frmPersonas";
		public final static String MANTENIMIENTO_LUGARES = "frmLugares";
		public final static String MANTENIMIENTO_DIANOHABIL = "frmDiaNoHabil";
		public final static String AYUDA_ACERCA_DE = "frmAcercaDe";
		public final static String MANTENIMIENTO_USUARIOS = "frmUsuarios";
		public final static String MANTENIMIENTO_CUIS_SUBTIPO = "frmCuisSubtipo";
		public final static String MANTENIMIENTO_CUIS_BASE ="frmCuisBase";
		public final static String MANTENIMIENTO_CUIS ="frmCuis";

	}


	public static abstract class MENSAJE {
		public final static Integer TYPE_ERROR = 3;
		public final static Integer TYPE_WARNING = 2;
		public final static Integer TYPE_SUCCES = 1;
	}

}
