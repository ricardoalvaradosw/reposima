package com.sima.servicio.impl;

import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.Grupo;
import com.sima.entidad.GrupoMenu;
import com.sima.entidad.Menu;
import com.sima.entidad.UsuarioAcceso;
import com.sima.repositorio.GrupoMenuRepositorio;
import com.sima.repositorio.GrupoRepositorio;
import com.sima.repositorio.MenuRepositorio;
import com.sima.servicio.GrupoMenuServicio;
import com.sima.servicio.GrupoService;
import com.sima.servicio.UsuarioAccesoServicio;
import com.sima.util.Busqueda;
import com.sima.util.Constantes;
import com.sima.util.Criterio;

@Service
public class GrupoServiceImpl extends BaseServicioImpl<Grupo, Long> implements
		GrupoService {

	@Autowired
	private GrupoRepositorio grupoRepositorio;

	@Autowired
	private MenuRepositorio menuRepositorio;

	@Autowired
	private GrupoMenuRepositorio grupoMenuRepositorio;

	@Autowired
	private GrupoMenuServicio grupoMenuServicio;

	@Autowired
	private UsuarioAccesoServicio usuarioAccesoServicio;

	@Autowired
	public GrupoServiceImpl(GrupoRepositorio grupoRepositorio) {
		super(grupoRepositorio);
		this.grupoRepositorio = grupoRepositorio;
	}

	@Override
	public Busqueda BuscarPorGrupo(Grupo grupo, Long paginaActual) {
		Busqueda busqueda = new Busqueda();
		Criterio filtro = Criterio.forClass(Grupo.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(Restrictions.ilike("descripcion", grupo.getDescripcion(),
				MatchMode.ANYWHERE));

		Long totalRegistros = grupoRepositorio.cantidadPorCriteria(filtro);
		if (totalRegistros % Constantes.PAGINACION.GRUPO == 0) {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.GRUPO);
		} else {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.GRUPO + 1);
		}
	
		if (paginaActual > busqueda.getNumeroPaginas()) {
			busqueda.setPaginaActual(busqueda.getNumeroPaginas());
		} else {
			busqueda.setPaginaActual(paginaActual);
		}
		if (paginaActual <= 0) {
			busqueda.setPaginaActual(1L);
		}
		filtro.setMaxResults(Constantes.PAGINACION.GRUPO.intValue());
		filtro.setFirstResult((busqueda.getPaginaActual().intValue() - 1)
				* Constantes.PAGINACION.GRUPO.intValue());
		filtro.addOrder(Order.asc("descripcion"));
		busqueda.setRegistos(grupoRepositorio.buscarPorCriteria(filtro));

		return busqueda;

	}

	@Override
	public void eliminiarGrupo(Grupo grupo) {
		// TODO Auto-generated method stub
		grupo.setEstado(Boolean.FALSE);
		grupoRepositorio.actualizar(grupo);
		List<GrupoMenu> grupoMenus = grupoMenuServicio.buscarPorCodGrupo(grupo.getId(),true);
		for(GrupoMenu grupoMenu: grupoMenus){
			grupoMenu.setEstado(Boolean.FALSE);
			grupoMenu.setMotivoModificacion(grupo.getMotivoModificacion());
			grupoMenuRepositorio.actualizar(grupoMenu);
		}
		
		List<UsuarioAcceso> usuarioAccesos= usuarioAccesoServicio.buscarPorCodGrupo(grupo.getId());
		for(UsuarioAcceso usuarioAcceso:usuarioAccesos){
			usuarioAcceso.setEstado(false);
			usuarioAcceso.setMotivoModificacion(grupo.getMotivoModificacion());
			usuarioAccesoServicio.actualizar(usuarioAcceso);
		}
	}

	@Override
	public boolean validarDuplicado(Grupo grupo) {
		Criterio filtro = Criterio.forClass(Grupo.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(Restrictions.eq("descripcion", grupo.getDescripcion()));
		if (grupo.getId() != null) {
			filtro.add(Restrictions.not(Restrictions.eq("id", grupo.getId())));
		}
		if (buscarPorCriteria(filtro).size() > 0) {
			return false;
		}
		return true;
	}

	@Override
	public void crearGrupo(Grupo grupo) {
		// TODO Auto-generated method stub
		 List<Menu> menus = menuRepositorio.obtenerTodos();
		 GrupoMenu grupoMenu;
		 grupoRepositorio.crear(grupo);
		 for(Menu menu:menus){
			 grupoMenu= new GrupoMenu();
			 grupoMenu.setAcceso(false);
			 grupoMenu.setNuevo(false);
			 grupoMenu.setModificar(false);
			 grupoMenu.setConsultar(false);
			 grupoMenu.setEliminar(false);
			 grupoMenu.setImprimir(false);
			 grupoMenu.setGrupo(grupo);
			 grupoMenu.setMenu(menu);
			 grupoMenuRepositorio.crear(grupoMenu);
		 }
	}
}
