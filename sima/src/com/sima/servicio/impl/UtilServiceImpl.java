package com.sima.servicio.impl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.sima.servicio.UtilService;
import com.sima.util.SimaUtil;

@Service
public class UtilServiceImpl implements UtilService {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Map<String, Integer> getLengthColumns(String tableName) {
		Connection connection = null;
		Map<String, Integer> columnLengthsMap = new HashMap<String, Integer>();
		try {
			connection = dataSource.getConnection();

			DatabaseMetaData metadata = connection.getMetaData();
			ResultSet resultSet = metadata.getColumns(null, null, tableName,
					null);
			while (resultSet.next()) {
				columnLengthsMap.put(resultSet.getString("COLUMN_NAME"),
						resultSet.getInt("COLUMN_SIZE"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return columnLengthsMap;
	}



}
