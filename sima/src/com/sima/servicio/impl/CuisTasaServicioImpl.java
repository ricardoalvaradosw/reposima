package com.sima.servicio.impl;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.CuisDetalle;
import com.sima.entidad.CuisTasa;
import com.sima.repositorio.CuisDetalleRepositorio;
import com.sima.repositorio.CuisTasaJDBCRepositorio;
import com.sima.repositorio.CuisTasaRepositorio;
import com.sima.servicio.CuisTasaServicio;
import com.sima.util.Busqueda;
import com.sima.util.Constantes;
import com.sima.util.Criterio;

@Service
public class CuisTasaServicioImpl extends BaseServicioImpl<CuisTasa, Long>
		implements CuisTasaServicio {

	@Autowired
	private CuisTasaRepositorio cuisTasaRepositorio;

	@Autowired
	private CuisDetalleRepositorio cuisDetalleRepositorio;
	
	@Autowired
	private CuisTasaJDBCRepositorio cuisTasaJDBCRepositorio;

	@Autowired
	public CuisTasaServicioImpl(CuisTasaRepositorio cuisTasaRepositorio) {
		super(cuisTasaRepositorio);
		this.cuisTasaRepositorio = cuisTasaRepositorio;
	}

	@Override
	public void guardarCuis(CuisDetalle cuisDetalle, List<CuisTasa> cuisTasaList) {
		// TODO Auto-generated method stub
		cuisDetalleRepositorio.crear(cuisDetalle);
		for (CuisTasa cuisTasa : cuisTasaList) {
			cuisTasa.setCuisDetalle(cuisDetalle);
			cuisTasaRepositorio.crear(cuisTasa);
		}
	}

	@Override
	public Busqueda BuscarPorCuis(CuisTasa cuisTasa, Long paginaActual) {
		// TODO Auto-generated method stub
		Busqueda busqueda = new Busqueda();
		Criterio filtro = Criterio.forClass(CuisTasa.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE))
		.createCriteria("cuisDetalle").add(Restrictions.eq("estado", Boolean.TRUE))
		.add(Restrictions.ilike("codigo", cuisTasa.getCuisDetalle().getCodigo(),MatchMode.ANYWHERE))
		.add(Restrictions.ilike("nombre", cuisTasa.getCuisDetalle().getNombre()));
		
		if(cuisTasa.getCuisBase() != null){
			filtro.createCriteria("cuisBase").add(Restrictions.eq("estado", Boolean.TRUE))
			.add(Restrictions.eq("id", cuisTasa.getCuisBase().getId()));
		}
		
		Long totalRegistros = cuisTasaRepositorio.cantidadPorCriteria(filtro);
		if (totalRegistros % Constantes.PAGINACION.CUIS == 0) {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUIS);
		} else {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUIS + 1);
		}

		if (paginaActual > busqueda.getNumeroPaginas()) {
			busqueda.setPaginaActual(busqueda.getNumeroPaginas());
		} else {
			busqueda.setPaginaActual(paginaActual);
		}
		if (paginaActual <= 0) {
			busqueda.setPaginaActual(1L);
		}
		filtro.setMaxResults(Constantes.PAGINACION.CUIS.intValue());
		filtro.setFirstResult((busqueda.getPaginaActual().intValue() - 1)
				* Constantes.PAGINACION.CUIS.intValue());
		busqueda.setRegistos(cuisTasaRepositorio.buscarPorCriteria(filtro));

		return busqueda;
		
	}

	@Override
	public Long ObtenerCantidadSubtipoxDetalle(CuisDetalle cuisDetalle) {
		// TODO Auto-generated method stub
		Criterio filtro = Criterio.forClass(CuisTasa.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE))
		.createCriteria("cuisDetalle").add(Restrictions.eq("estado", Boolean.TRUE))
		.add(Restrictions.eq("id",cuisDetalle.getId()));
		return cuisTasaRepositorio.cantidadPorCriteria(filtro);
	}

	@Override
	public String ObtenerBasexDetalle(CuisDetalle cuisDetalle) {
		// TODO Auto-generated method stub
		Criterio filtro = Criterio.forClass(CuisTasa.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.createCriteria("cuisDetalle").add(Restrictions.eq("estado", Boolean.TRUE))
		.add(Restrictions.eq("id",cuisDetalle.getId()));
		
		List<CuisTasa> lista = buscarPorCriteria(filtro);
		for(int i = 0 ; i<lista.size() ; i++){
			if(lista.get(i).getCuisBase().getAbrev().trim().equalsIgnoreCase("UIT")){
				
				return "UIT";
			}
		}
		return lista.get(0).getCuisBase().getAbrev().trim();
		
	
	}

	@Override
	public List<CuisTasa> obtenerTasaxDetalle(CuisDetalle cuisDetalle) {
		// TODO Auto-generated method stub
		Criterio filtro = Criterio.forClass(CuisTasa.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE))
		.createCriteria("cuisDetalle").add(Restrictions.eq("estado", Boolean.TRUE))
		.add(Restrictions.eq("id",cuisDetalle.getId()));
		return buscarPorCriteria(filtro);
	}

	@Override
	public void EliminarCuis(CuisDetalle cuisDetalle, List<CuisTasa> cuisTasaList) {
		// TODO Auto-generated method stub
		cuisDetalle.setEstado(Boolean.FALSE);
		cuisDetalleRepositorio.actualizar(cuisDetalle);
		for(CuisTasa ct : cuisTasaList){
			ct.setEstado(Boolean.FALSE);
			cuisTasaRepositorio.actualizar(ct);
		}
	}

	@Override
	public void ActualizarCuis(CuisDetalle cuisDetalle, List<CuisTasa> cuisTasas) {
		// TODO Auto-generated method stub
		cuisDetalle.setEstado(Boolean.TRUE);
		cuisDetalleRepositorio.actualizar(cuisDetalle);
		for(CuisTasa ct: cuisTasas){
			ct.setEstado(Boolean.TRUE);
			cuisTasaRepositorio.actualizar(ct);
		}
	}

	@Override
	public BigDecimal getUITxAnio(String anio) {
		// TODO Auto-generated method stub
		return cuisTasaJDBCRepositorio.getUITPorAnio(anio);
	}

}
