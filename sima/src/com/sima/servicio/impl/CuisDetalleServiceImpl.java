package com.sima.servicio.impl;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.FetchMode;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.CuisDetalle;
import com.sima.repositorio.CuisDetalleRepositorio;
import com.sima.servicio.CuisDetalleServicio;
import com.sima.util.Busqueda;
import com.sima.util.Constantes;
import com.sima.util.Criterio;

@Service
public class CuisDetalleServiceImpl extends BaseServicioImpl<CuisDetalle, Long>
		implements CuisDetalleServicio {

	@Autowired
	private CuisDetalleRepositorio cuisDetalleRepositorio;

	@Autowired
	public CuisDetalleServiceImpl(CuisDetalleRepositorio cuisDetalleRepositorio) {
		super(cuisDetalleRepositorio);
		this.cuisDetalleRepositorio = cuisDetalleRepositorio;
	}

	@Override
	public Busqueda BuscarPorCuisDetalle(CuisDetalle cuisDetalle,
			Long paginaActual,Long codBase) {
		// TODO Auto-generated method stub
		Busqueda busqueda = new Busqueda();
		Criterio filtro = Criterio.forClass(CuisDetalle.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(
				Restrictions.ilike("nombre", cuisDetalle.getNombre(),
						MatchMode.ANYWHERE)).add(
				Restrictions.ilike("codigo", cuisDetalle.getCodigo(),
						MatchMode.ANYWHERE));
		Criterio tasaCriterio = filtro.setFetchMode("cuisTasaList",
				FetchMode.JOIN).createCriteria("cuisTasaList");
		tasaCriterio.createCriteria("cuisBase").add(Restrictions.eq("id", codBase ));

		Long totalRegistros = cuisDetalleRepositorio
				.cantidadPorCriteria(filtro);
		if (totalRegistros % Constantes.PAGINACION.CUIS!= 0) {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUIS + 1);
		} else {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUIS);
		}
		if (paginaActual > busqueda.getNumeroPaginas()) {
			busqueda.setPaginaActual(busqueda.getNumeroPaginas());
		} else {
			busqueda.setPaginaActual(paginaActual);
		}
		if (paginaActual <= 0) {
			busqueda.setPaginaActual(1L);
		}
		filtro.setMaxResults(Constantes.PAGINACION.CUIS.intValue());
		filtro.setFirstResult((busqueda.getPaginaActual().intValue() - 1)
				* Constantes.PAGINACION.CUIS.intValue());
		
		List<CuisDetalle> lista = buscarPorCriteria(filtro);
		Set<CuisDetalle> linkedHashSet = new LinkedHashSet<CuisDetalle>();
		linkedHashSet.addAll(lista);
		lista.clear();
		lista.addAll(linkedHashSet);
		busqueda.setRegistos(lista);
		return busqueda;
	
	}

	@Override
	public Boolean ValidarCodigoRepetido(String codigo) {
		// TODO Auto-generated method stub
		Criterio filtro = Criterio.forClass(CuisDetalle.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(Restrictions.eq("codigo", codigo));
		List<CuisDetalle> lista = buscarPorCriteria(filtro);
		if(lista.size() > 0){
			return true;
		}
		return false;
	}


}
