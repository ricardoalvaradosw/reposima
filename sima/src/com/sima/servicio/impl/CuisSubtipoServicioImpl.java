package com.sima.servicio.impl;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.CuisBase;
import com.sima.entidad.CuisSubtipo;
import com.sima.repositorio.CuisSubtipoRepositorio;
import com.sima.servicio.CuisSubtipoServicio;
import com.sima.util.Busqueda;
import com.sima.util.Constantes;
import com.sima.util.Criterio;

@Service
public class CuisSubtipoServicioImpl extends
		BaseServicioImpl<CuisSubtipo, Long> implements CuisSubtipoServicio {

	@Autowired
	private CuisSubtipoRepositorio cuisCubtipoRepositorio;

	@Autowired
	public CuisSubtipoServicioImpl(CuisSubtipoRepositorio cuisCubtipoRepositorio) {
		super(cuisCubtipoRepositorio);
		this.cuisCubtipoRepositorio = cuisCubtipoRepositorio;

	}

	@Override
	public Busqueda BuscarPorCuisSubtipo(CuisSubtipo cuisSubtipo,
			Long paginaActual) {
		Busqueda busqueda = new Busqueda();
		Criterio filtro = Criterio.forClass(CuisSubtipo.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(Restrictions.ilike("nombre", cuisSubtipo.getNombre(),
				MatchMode.ANYWHERE));

		Long totalRegistros = cuisCubtipoRepositorio.cantidadPorCriteria(filtro);
		if (totalRegistros % Constantes.PAGINACION.CUISSUBTIPO == 0) {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUISSUBTIPO);
		} else {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUISSUBTIPO + 1);
		}

		if (paginaActual > busqueda.getNumeroPaginas()) {
			busqueda.setPaginaActual(busqueda.getNumeroPaginas());
		} else {
			busqueda.setPaginaActual(paginaActual);
		}
		if (paginaActual <= 0) {
			busqueda.setPaginaActual(1L);
		}
		filtro.setMaxResults(Constantes.PAGINACION.CUISSUBTIPO.intValue());
		filtro.setFirstResult((busqueda.getPaginaActual().intValue() - 1)
				* Constantes.PAGINACION.CUISSUBTIPO.intValue());
		filtro.addOrder(Order.asc("nombre"));
		busqueda.setRegistos(cuisCubtipoRepositorio.buscarPorCriteria(filtro));

		return busqueda;
	}

	@Override
	public void EliminarCuisSubtipo(CuisSubtipo cuisSubtipo) {
		// TODO Auto-generated method stub
		cuisSubtipo.setEstado(Boolean.FALSE);
		actualizar(cuisSubtipo);
		;
	}

}
