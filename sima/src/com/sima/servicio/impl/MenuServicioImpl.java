package com.sima.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.Menu;
import com.sima.repositorio.MenuRepositorio;
import com.sima.servicio.MenuServicio;

@Service
public class MenuServicioImpl extends BaseServicioImpl<Menu, Long> implements MenuServicio{

	@Autowired
	public MenuServicioImpl(MenuRepositorio menuRepositorio) {
		super(menuRepositorio);
	}

}
