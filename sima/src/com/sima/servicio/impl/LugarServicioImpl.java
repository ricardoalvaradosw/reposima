package com.sima.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.Lugar;
import com.sima.entidad.Persona;
import com.sima.repositorio.LugarRepositorio;
import com.sima.servicio.LugarServicio;

@Service
public class LugarServicioImpl extends BaseServicioImpl<Lugar, String> implements LugarServicio{

	@Autowired
	private LugarRepositorio lugarRepositorio;
	
	@Autowired
	public LugarServicioImpl(LugarRepositorio lugarRepositorio){
		super(lugarRepositorio);
		this.lugarRepositorio= lugarRepositorio;
	}

	@Override
	public Lugar obtenerLugarPorPersona(Persona persona) {
		// TODO Auto-generated method stub
		return lugarRepositorio.obtenerLugarByPersona(persona);
	}
}
