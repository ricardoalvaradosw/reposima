package com.sima.servicio.impl;

import java.util.List;








import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.Persona;
import com.sima.gui.PersonaBuscar;
import com.sima.repositorio.PersonaRepositorio;
import com.sima.servicio.PersonaServicio;
import com.sima.util.Busqueda;
import com.sima.util.Constantes;
import com.sima.util.Criterio;
@Service
public class PersonaServicioImpl extends BaseServicioImpl<Persona, String> implements PersonaServicio {
	protected static final Log logger = LogFactory
			.getLog(PersonaServicioImpl.class);
	
	@Autowired
	private PersonaRepositorio personaRepositorio;
	
	@Autowired
	public PersonaServicioImpl(PersonaRepositorio personaRepositorio){
		super(personaRepositorio);
		this.personaRepositorio=personaRepositorio;
	}

	@Override
	public Busqueda buscarPorPersona(Persona persona, Long paginaActual) {
		// TODO Auto-generated method stub
		Busqueda busqueda = new Busqueda();
		// Preparamos para obtener la cantidad de paginas
		/*Criterio filtro = Criterio.forClass(Persona.class,"persona");
		filtro.setFetchMode("persona.documentosIdentidad",FetchMode.JOIN);
		filtro.createAlias("persona.documentosIdentidad","documentos");
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(Restrictions.ilike("documentos.numero", persona.getDocumentosIdentidad().get(0).getNumero(),MatchMode.ANYWHERE));
		filtro.add(Restrictions.ilike("persona.nombre", persona.getNombre(),
				MatchMode.ANYWHERE));
		filtro.setProjection(Projections.projectionList()
				.add(Projections.property("idPersona").as("idPersona"))
				.add(Projections.property("nombre").as("nombre"))
				.add(Projections.property("numero").as("numero"))
				.add(Projections.property("departamento").as("departamento"))
				.add(Projections.property("piso").as("piso"))
				.add(Projections.property("bloque").as("bloque"))
				.add(Projections.property("manzana").as("manzana"))
				.add(Projections.property("lote").as("lote")));
		
	
		
	
		filtro.setResultTransformer(new AliasToBeanResultTransformer(Persona.class));
		filtro.setResultTransformer(Transformers.aliasToBean(Persona.class));
		filtro.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		Long totalRegistros = personaRepositorio.cantidadPorCriteria(filtro);
		*/
		Map<Object,Object> result = personaRepositorio.getQueryForSearch(persona);
		Query query = (Query) result.get("query");

		Long totalRegistros = (Long)(result.get("size"));
		if (totalRegistros % Constantes.PAGINACION.PERSONA == 0) {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.PERSONA);
		} else {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.PERSONA + 1);
		}
		// validaciones
		if (paginaActual > busqueda.getNumeroPaginas()) {
			busqueda.setPaginaActual(busqueda.getNumeroPaginas());
		} else {
			busqueda.setPaginaActual(paginaActual);
		}
		if (paginaActual <= 0) {
			busqueda.setPaginaActual(1L);
		}
		// /
		// obtenemos los registros de la pagina solicitada
		query.setMaxResults(Constantes.PAGINACION.PERSONA.intValue());
		query.setFirstResult((busqueda.getPaginaActual().intValue() - 1));
		
				//		* Constantes.PAGINACION.PERSONA.intValue())
		//filtro.setMaxResults(Constantes.PAGINACION.PERSONA.intValue());
		//filtro.setFirstResult((busqueda.getPaginaActual().intValue() - 1)
		//		* Constantes.PAGINACION.PERSONA.intValue());
		//filtro.addOrder(Order.asc("nombre"));
		//busqueda.setRegistos(personaRepositorio.findByCriteria(filtro));

		busqueda.setRegistos(query.list());
		return busqueda;
	}

}
