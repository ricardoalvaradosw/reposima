package com.sima.servicio.impl;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.CuisBase;
import com.sima.entidad.Grupo;
import com.sima.repositorio.CuisBaseRepositorio;
import com.sima.servicio.CuisBaseServicio;
import com.sima.util.Busqueda;
import com.sima.util.Constantes;
import com.sima.util.Criterio;

@Service
public class CuisBaseServicioImpl extends BaseServicioImpl<CuisBase, Long>
		implements CuisBaseServicio {

	@Autowired
	private CuisBaseRepositorio cuisBaseRepositorio;

	@Autowired
	public CuisBaseServicioImpl(CuisBaseRepositorio cuisBaseRepositorio) {
		super(cuisBaseRepositorio);
		this.cuisBaseRepositorio = cuisBaseRepositorio;
	}

	@Override
	public Busqueda BuscarPorCuisBase(CuisBase cuisBase, Long paginaActual) {
		Busqueda busqueda = new Busqueda();
		Criterio filtro = Criterio.forClass(CuisBase.class);
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(
				Restrictions.ilike("nombre", cuisBase.getNombre(),
						MatchMode.ANYWHERE)).add(
				Restrictions.ilike("abrev", cuisBase.getAbrev(),
						MatchMode.ANYWHERE));

		Long totalRegistros = cuisBaseRepositorio.cantidadPorCriteria(filtro);
		if (totalRegistros % Constantes.PAGINACION.CUISBASE == 0) {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUISBASE);
		} else {
			busqueda.setNumeroPaginas(totalRegistros
					/ Constantes.PAGINACION.CUISBASE + 1);
		}

		if (paginaActual > busqueda.getNumeroPaginas()) {
			busqueda.setPaginaActual(busqueda.getNumeroPaginas());
		} else {
			busqueda.setPaginaActual(paginaActual);
		}
		if (paginaActual <= 0) {
			busqueda.setPaginaActual(1L);
		}
		filtro.setMaxResults(Constantes.PAGINACION.CUISBASE.intValue());
		filtro.setFirstResult((busqueda.getPaginaActual().intValue() - 1)
				* Constantes.PAGINACION.CUISBASE.intValue());
		filtro.addOrder(Order.asc("nombre"));
		busqueda.setRegistos(cuisBaseRepositorio.buscarPorCriteria(filtro));

		return busqueda;
	}

	@Override
	public void EliminarCuisBase(CuisBase cuisBase) {
		// TODO Auto-generated method stub
		cuisBase.setEstado(Boolean.FALSE);
		cuisBaseRepositorio.actualizar(cuisBase);
	}

}
