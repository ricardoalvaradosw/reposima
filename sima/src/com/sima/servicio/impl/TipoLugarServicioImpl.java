package com.sima.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Service;

import com.sima.entidad.Lugar;
import com.sima.entidad.TipoLugar;
import com.sima.repositorio.TipoLugarRepositorio;
import com.sima.servicio.TipoLugarServicio;
@Service
public class TipoLugarServicioImpl  extends BaseServicioImpl<TipoLugar, Integer> implements TipoLugarServicio{

	@Autowired
	private TipoLugarRepositorio tipoLugarRepositorio;
	
	@Autowired
	public TipoLugarServicioImpl(TipoLugarRepositorio tipoLugarRepositorio){
		super(tipoLugarRepositorio);
		this.tipoLugarRepositorio=tipoLugarRepositorio;
	}

	@Override
	public TipoLugar obtenerTipoLugarporLugar(Lugar lugar) {
		// TODO Auto-generated method stub
		return tipoLugarRepositorio.obtenerTipoLugarByLugar(lugar);
	}
}
