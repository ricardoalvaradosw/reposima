package com.sima.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.Via;
import com.sima.repositorio.ViaRepositorio;
import com.sima.servicio.ViaServicio;
@Service
public class ViaServicioImpl extends BaseServicioImpl<Via, String> implements ViaServicio{
	
	@Autowired
	private ViaRepositorio viaRepositorio;

	@Autowired
	public ViaServicioImpl(ViaRepositorio viaRepositorio){
		super(viaRepositorio);
		this.viaRepositorio = viaRepositorio;
	}
	
}
