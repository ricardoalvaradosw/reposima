package com.sima.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.DocumentoPersona;
import com.sima.entidad.DocumentoPersonaId;
import com.sima.entidad.Persona;
import com.sima.repositorio.DocumentoPersonaRepositorio;
import com.sima.servicio.DocumentoPersonaServicio;

@Service
public class DocumentoPersonaServicioImpl extends BaseServicioImpl<DocumentoPersona, DocumentoPersonaId> implements DocumentoPersonaServicio {

	@Autowired
	private DocumentoPersonaRepositorio documentoPersonaRepositorio;
	
	@Autowired
	public DocumentoPersonaServicioImpl( DocumentoPersonaRepositorio documentoPersonaRepositorio){
		super(documentoPersonaRepositorio);
		this.documentoPersonaRepositorio=documentoPersonaRepositorio;
	}

	@Override
	public DocumentoPersona obtenerDocumentoPorPersona(Persona persona) {
		// TODO Auto-generated method stub
		return documentoPersonaRepositorio.obtenerDocumentoByPersona(persona);
	}
}
