package com.sima.servicio.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.GrupoMenu;
import com.sima.repositorio.GrupoMenuRepositorio;
import com.sima.servicio.GrupoMenuServicio;
import com.sima.util.Criterio;

@Service
public class GrupoMenuServicioImpl extends
		BaseServicioImpl<GrupoMenu, Long> implements GrupoMenuServicio {
	
	@Autowired
	private GrupoMenuRepositorio grupoMenuRepositorio;

	@Autowired
	public GrupoMenuServicioImpl(GrupoMenuRepositorio grupoMenuRepositorio) {
		super(grupoMenuRepositorio);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<GrupoMenu> buscarPorCodGrupo(Long codigoGrupo,Boolean todos) {
		Criterio filtro = Criterio.forClass(GrupoMenu.class);
		if(!todos){
		filtro.add(Restrictions.eq("acceso", Boolean.TRUE));
		}
		filtro.add(Restrictions.eq("estado", Boolean.TRUE));
		filtro.add(Restrictions.eq("grupo.id", codigoGrupo));
		List<GrupoMenu> grupoMenu = buscarPorCriteria(filtro);
		return grupoMenu;
	
	}

	@Override
	public void actualizarTodos(List<GrupoMenu> grupoMenusActualizar) {
		for(GrupoMenu grupoMenu:grupoMenusActualizar){
			super.actualizar(grupoMenu);
		}
		
	}
	
	
}
