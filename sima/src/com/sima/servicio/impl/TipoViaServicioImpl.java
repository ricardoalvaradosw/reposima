package com.sima.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.entidad.TipoVia;
import com.sima.repositorio.TipoViaRepositorio;
import com.sima.servicio.TipoViaServicio;
@Service
public class TipoViaServicioImpl extends BaseServicioImpl<TipoVia,Short> implements TipoViaServicio {

	@Autowired
	private TipoViaRepositorio tipoViaRepositorio;
	
	@Autowired
	public TipoViaServicioImpl(TipoViaRepositorio tipoViaRepositorio){
		super(tipoViaRepositorio);
		this.tipoViaRepositorio= tipoViaRepositorio;
	}
	
}
