package com.sima.servicio.impl;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sima.repositorio.ReporteRepositorio;
import com.sima.servicio.ReporteServicio;
import com.sima.util.JasperExport;
import com.sima.util.TipoFormatoExportacion;


@Service
public class ReporteServicioImpl implements ReporteServicio {

	@Autowired
	private ReporteRepositorio reporteRepositorio; 
	
	public byte[] obtenerReporte(String reportName, String formatType,Map<String, Object> parameters) {
		byte[] response=null;
		JasperPrint jasperPrint=reporteRepositorio.generarReporte(reportName, parameters);
		if(TipoFormatoExportacion.PDF.getValue().equals(formatType)){
			response=JasperExport.pdfReportToArray(jasperPrint);
		}else if(TipoFormatoExportacion.EXCEL.getValue().equals(formatType)){
			response=JasperExport.xlsReportToArray(jasperPrint);
		}else if(TipoFormatoExportacion.WORD.getValue().equals(formatType)){
			response=JasperExport.rtfReportToArray(jasperPrint);
		}
		return response;
	}

}