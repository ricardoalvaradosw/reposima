 package com.sima.servicio;

import java.math.BigDecimal;
import java.util.List;

import com.sima.entidad.CuisDetalle;
import com.sima.entidad.CuisTasa;
import com.sima.util.Busqueda;

public interface CuisTasaServicio extends BaseServicio<CuisTasa, Long> {
	public void guardarCuis(CuisDetalle cuisDetalle, List<CuisTasa> cuisTasa);
	public Busqueda BuscarPorCuis(CuisTasa cuisTasa, Long paginaActual);
	public Long ObtenerCantidadSubtipoxDetalle(CuisDetalle cuisDetalle);
	public String ObtenerBasexDetalle(CuisDetalle cuisDetalle);
	public List<CuisTasa> obtenerTasaxDetalle(CuisDetalle cuisDetalle);
	public void EliminarCuis(CuisDetalle cuisDetalle, List<CuisTasa> cuisTasa);
	public void ActualizarCuis(CuisDetalle cuisDetalle, List<CuisTasa> cuisTasas);
	public BigDecimal getUITxAnio(String anio);
}
