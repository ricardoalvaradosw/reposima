package com.sima.servicio;

import com.sima.entidad.Grupo;
import com.sima.repositorio.BaseRepositorio;
import com.sima.util.Busqueda;

public interface GrupoService extends BaseServicio<Grupo, Long>{
	
	public Busqueda BuscarPorGrupo(Grupo grupo, Long paginaActual);
	public void eliminiarGrupo(Grupo grupo);
	public void crearGrupo(Grupo grupo);
	public boolean validarDuplicado(Grupo grupo);

}
