package com.sima.servicio;

import com.sima.entidad.CuisBase;
import com.sima.util.Busqueda;

public interface CuisBaseServicio extends BaseServicio<CuisBase, Long> {
	public Busqueda BuscarPorCuisBase(CuisBase cuisBase, Long paginaActual);
	public void EliminarCuisBase(CuisBase cuisBase);
}
