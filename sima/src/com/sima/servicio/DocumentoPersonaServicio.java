package com.sima.servicio;

import com.sima.entidad.DocumentoPersona;
import com.sima.entidad.DocumentoPersonaId;
import com.sima.entidad.Persona;

public interface DocumentoPersonaServicio extends BaseServicio<DocumentoPersona, DocumentoPersonaId>{
	public DocumentoPersona obtenerDocumentoPorPersona(Persona persona);
}
