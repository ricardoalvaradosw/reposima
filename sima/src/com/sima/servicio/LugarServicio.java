package com.sima.servicio;

import com.sima.entidad.Lugar;
import com.sima.entidad.Persona;

public interface LugarServicio extends BaseServicio<Lugar, String> {
	
	public Lugar obtenerLugarPorPersona(Persona persona);

}
