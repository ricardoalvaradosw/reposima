package com.sima.servicio;

import com.sima.entidad.CuisDetalle;
import com.sima.util.Busqueda;

public interface CuisDetalleServicio extends BaseServicio<CuisDetalle, Long> {
	public Busqueda BuscarPorCuisDetalle(CuisDetalle cuisDetalle, Long paginaActual,Long codBase);
	public Boolean ValidarCodigoRepetido(String codigo);

}
