package com.sima.servicio;

import com.sima.entidad.Persona;
import com.sima.util.Busqueda;

public interface PersonaServicio extends BaseServicio<Persona, String> {
	
	public Busqueda buscarPorPersona(Persona persona, Long paginaActual);

}
