package com.sima.servicio;

import java.util.List;
import java.util.Map;


public interface ReporteServicio {
	
	public byte[] obtenerReporte(String reportName,String formatType,Map<String,Object> parameters);
	
}