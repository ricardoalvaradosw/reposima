package com.sima.servicio;

import com.sima.entidad.Lugar;
import com.sima.entidad.TipoLugar;


public interface TipoLugarServicio extends BaseServicio<TipoLugar, Integer> {

	public TipoLugar obtenerTipoLugarporLugar(Lugar lugar);
}
