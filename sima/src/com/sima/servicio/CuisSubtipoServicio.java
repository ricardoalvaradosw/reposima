package com.sima.servicio;

import com.sima.entidad.CuisSubtipo;
import com.sima.util.Busqueda;

public interface CuisSubtipoServicio extends BaseServicio<CuisSubtipo, Long>  {
	
	public Busqueda BuscarPorCuisSubtipo(CuisSubtipo cuisSubtipo, Long paginaActual);
	public void EliminarCuisSubtipo(CuisSubtipo cuisSubtipo);

}
