package com.sima.gui;

import java.util.List;
import java.util.Map;

import org.vaadin.dialogs.ConfirmDialog;

import com.sima.entidad.Grupo;
import com.sima.gui.ui.NumberField;
import com.sima.servicio.GrupoService;
import com.sima.servicio.UtilService;
import com.sima.util.Boton;
import com.sima.util.Busqueda;
import com.sima.util.Constantes;
import com.sima.util.Injector;
import com.sima.util.Notificacion;
import com.sima.util.Permiso;
import com.sima.util.SimaUtil;
import com.sima.util.TextField;
import com.vaadin.annotations.AutoGenerated;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

public class GrupoMantenimiento extends CustomComponent implements
		ClickListener {

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	
	@AutoGenerated
	private AbsoluteLayout mainLayout;
	@AutoGenerated
	private Label lblTotalPaginas;
	@AutoGenerated
	private Label label;
	@AutoGenerated
	private NumberField txtPaginaActual;
	@AutoGenerated
	private Boton btnAnterior;
	@AutoGenerated
	private Boton btnFinal;
	@AutoGenerated
	private Boton btnSiguiente;
	@AutoGenerated
	private Boton btnInicio;
	@AutoGenerated
	private TextField txtDescripcionBuscar;
	@AutoGenerated
	private Button btnCancelar;
	@AutoGenerated
	private Button btnImprimir;
	@AutoGenerated
	private Button btnEliminar;
	@AutoGenerated
	private Button btnGuardar;
	@AutoGenerated
	private Button btnNuevo;
	@AutoGenerated
	private TextField txtDescripcion;
	@AutoGenerated
	private Label lblDescripcion;
	@AutoGenerated
	private Label lblTitulo;
	@AutoGenerated
	private Table tbGrupos;
	private Window windowContiene;
	private CustomComponent quienLlama;
	private Permiso permiso;
	private Grupo grupo;
	private Boolean esNuevo;
	private GrupoService grupoService;
	private UtilService utilService;
	private Map<String, Integer> columnLenghts;

	/**
	 * The constructor should first build the main layout, set the composition
	 * root and then do any custom initialization.
	 *
	 * The constructor will not be automatically regenerated by the visual
	 * editor.
	 */
	public GrupoMantenimiento(Window windowContiene,
			CustomComponent quienLlama, Permiso permiso) {
		this.windowContiene = windowContiene;
		this.quienLlama = quienLlama;
		this.permiso = permiso;
		grupoService = Injector.obtenerServicio(GrupoService.class);
		utilService = Injector.obtenerServicio(UtilService.class);
		buildMainLayout();
		setCompositionRoot(mainLayout);
		postBuild();
		resetearFormulario();
		getAllGrupos();

		// TODO add user code here
	}

	public void postBuild() {
		btnAnterior.setStyleName(ValoTheme.BUTTON_LINK);
		btnInicio.setStyleName(ValoTheme.BUTTON_LINK);
		btnSiguiente.setStyleName(ValoTheme.BUTTON_LINK);
		btnFinal.setStyleName(ValoTheme.BUTTON_LINK);
		 lblDescripcion.setContentMode(ContentMode.HTML);
		 lblTitulo.setContentMode(ContentMode.HTML);
		columnLenghts = utilService.getLengthColumns("grupo");
		this.txtDescripcionBuscar
				.setMaxLength(columnLenghts.get("descripcion"));
		this.txtDescripcion.setMaxLength(columnLenghts.get("descripcion"));
		this.btnNuevo.addClickListener((ClickListener) this);
		this.btnGuardar.addClickListener((ClickListener) this);
		this.btnCancelar.addClickListener((ClickListener) this);
		this.btnEliminar.addClickListener((ClickListener) this);
		this.btnInicio.addClickListener((ClickListener) this);
		this.btnAnterior.addClickListener((ClickListener) this);
		this.btnSiguiente.addClickListener((ClickListener) this);
		this.btnFinal.addClickListener((ClickListener) this);
		this.btnImprimir.addClickListener((ClickListener) this);
		this.txtPaginaActual.setValue("1");

		IndexedContainer contenedor = new IndexedContainer();
		contenedor.addContainerProperty("id", Long.class, null);
		contenedor.addContainerProperty("item", Long.class, null);
		contenedor.addContainerProperty("descripcion", String.class, null);
		tbGrupos.setContainerDataSource(contenedor);
		tbGrupos.setVisibleColumns(new Object[] { "item", "descripcion" });
		tbGrupos.setColumnWidth("item", 30);
		tbGrupos.setColumnWidth("descripcion", 350);

		tbGrupos.setColumnHeader("item", "N�");
		tbGrupos.setColumnHeader("descripcion", "Descripcion");

		tbGrupos.setImmediate(true);
		tbGrupos.setSelectable(true);
		this.btnCancelar.setId("cancelar");
		this.btnEliminar.setId("eliminar");
		this.btnGuardar.setId("guardar");
		this.btnNuevo.setId("nuevo");
		this.btnImprimir.setId("imprimir");
		SimaUtil.validarBotones(permiso, btnNuevo, btnEliminar, btnCancelar,
				btnGuardar, btnImprimir);
		txtDescripcionBuscar.setVisible(permiso.getConsultar());
		tbGrupos.addValueChangeListener(new Property.ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				// TODO Auto-generated method stub
				Item item = tbGrupos.getItem(tbGrupos.getValue());
				if (item != null) {
					btnGuardar.setCaption("Actualizar");
					btnEliminar.setEnabled(true);
					btnCancelar.setVisible(permiso.getModificar());
					btnCancelar.setEnabled(true);
					btnNuevo.setEnabled(true);
					esNuevo = false;
					btnGuardar.setVisible(permiso.getModificar());
					btnGuardar.setEnabled(true);
					activarCampos(permiso.getModificar());
					getGrupo(new Long(item.getItemProperty("id").getValue()
							.toString()));
					tbGrupos.unselect(tbGrupos.getValue());
				}
			}
		});

		this.txtPaginaActual.setId("paginaActual");
		this.txtPaginaActual.setImmediate(true);
		this.txtPaginaActual.addShortcutListener(new ShortcutListener("",
				KeyCode.ENTER, null) {
			private static final long serialVersionUID = 1L;

			@Override
			public void handleAction(Object sender, Object target) {
				if (target instanceof TextField
						&& ((TextField) target).getId().equals("paginaActual")) {
					Long paginaActual = null;
					try {
						paginaActual = Long.parseLong(txtPaginaActual
								.getValue().toString());
					} catch (Exception exception) {
					}
					txtPaginaActual.setValue(paginaActual.toString());
					getAllGrupos();
				}
			}
		});

		this.txtDescripcionBuscar.setImmediate(true);
		this.txtDescripcionBuscar
				.addTextChangeListener(new TextChangeListener() {
					@Override
					public void textChange(TextChangeEvent event) {
						// TODO Auto-generated method stub
						txtPaginaActual.setValue("1");
						txtDescripcionBuscar.setValue(event.getText());
						getAllGrupos();
					}
				});
	}

	@SuppressWarnings("unchecked")
	public void getAllGrupos() {
		IndexedContainer contenedor = (IndexedContainer) tbGrupos
				.getContainerDataSource();
		contenedor.removeAllItems();

		Long paginaActual = Long.parseLong(this.txtPaginaActual.getValue()
				.toString());
		grupo = new Grupo();
		grupo.setDescripcion(this.txtDescripcionBuscar.getValue().toString());
		List<Grupo> grupos = null;
		Busqueda busqueda = grupoService.BuscarPorGrupo(grupo, paginaActual);
		this.txtPaginaActual.setValue(busqueda.getPaginaActual().toString());
		this.lblTotalPaginas.setValue(busqueda.getNumeroPaginas().toString());
		grupos = (List<Grupo>) busqueda.getRegistos();
		Long numeroItem = (Long.parseLong(this.txtPaginaActual.getValue()
				.toString()) - 1) * Constantes.PAGINACION.GRUPO + 1;

		for (int i = 0; i < grupos.size(); i++) {
			Item item = tbGrupos.addItem(i);
			item.getItemProperty("id").setValue(grupos.get(i).getId());
			item.getItemProperty("item").setValue(numeroItem++);
			item.getItemProperty("descripcion").setValue(
					grupos.get(i).getDescripcion());
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource() == this.btnNuevo) {
			NuevoRegistro();
			this.txtDescripcion.focus();
		} else if (event.getSource() == this.btnGuardar) {
			if (SimaUtil.validarCamposTexto(this.txtDescripcion)) {
				grupo.setDescripcion(this.txtDescripcion.getValue().toString());
				if (!grupoService.validarDuplicado(grupo)) {
					Notificacion.show(new Notificacion("El grupo ya existe",
							Constantes.MENSAJE.TYPE_ERROR));
				} else {
					if (this.esNuevo) {
						grupoService.crearGrupo(grupo);
						Notificacion.show(new Notificacion(
								"Se guardo correctamente el grupo",
								Constantes.MENSAJE.TYPE_SUCCES));
					} else {
						grupoService.actualizar(grupo);
						Notificacion.show(new Notificacion(
								"Se actualizo correctamente el grupo",
								Constantes.MENSAJE.TYPE_SUCCES));
					}
				}
				resetearFormulario();
				getAllGrupos();
			} else {
				Notificacion.show(new Notificacion(
						"Debe rellenar todos los campos",
						Constantes.MENSAJE.TYPE_WARNING));
			}
		} else if (event.getSource() == this.btnCancelar) {
			resetearFormulario();
		} else if (event.getSource() == this.btnEliminar) {
			ConfirmDialog.show(UI.getCurrent(), "Confirmaci�n",
					"�Desea elminar el Grupo?", "Aceptar", "Cancelar",
					new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 1L;

						public void onClose(ConfirmDialog dialog) {
							if (dialog.isConfirmed()) {
								grupoService.eliminiarGrupo(grupo);
								resetearFormulario();
								getAllGrupos();

							}
						}
					});
		} else if (event.getSource() == this.btnSiguiente) {
			Long paginaActual = Long.parseLong(this.txtPaginaActual.getValue()
					.toString()) + 1;
			if (paginaActual <= Long.parseLong(this.lblTotalPaginas.getValue()
					.toString())) {
				this.txtPaginaActual.setValue(paginaActual.toString());
			}
			getAllGrupos();
		} else if (event.getSource() == this.btnAnterior) {
			Long paginaActual = Long.parseLong(this.txtPaginaActual.getValue()
					.toString()) - 1;
			if (paginaActual >= 1) {
				this.txtPaginaActual.setValue(paginaActual.toString());
			}
			getAllGrupos();
		} else if (event.getSource() == this.btnInicio) {
			this.txtPaginaActual.setValue("1");
			getAllGrupos();
		} else if (event.getSource() == this.btnFinal) {
			this.txtPaginaActual.setValue(this.lblTotalPaginas.getValue());
			getAllGrupos();
		}

	}

	public void getGrupo(Long codGrupo) {
		grupo = grupoService.obtener(codGrupo);
		txtDescripcion.setValue(grupo.getDescripcion());
	}

	public void resetearFormulario() {
		this.esNuevo = false;
		this.btnNuevo.setEnabled(Boolean.TRUE);
		this.btnCancelar.setEnabled(Boolean.FALSE);
		this.btnGuardar.setEnabled(Boolean.FALSE);
		this.btnEliminar.setEnabled(Boolean.FALSE);
		this.txtDescripcion.setValue("");
		activarCampos(Boolean.FALSE);
	}

	public void NuevoRegistro() {
		grupo = new Grupo();
		this.esNuevo = Boolean.TRUE;
		this.btnGuardar.setVisible(this.permiso.getNuevo());
		this.btnGuardar.setCaption("Guardar");
		this.btnGuardar.setEnabled(true);
		this.btnCancelar.setVisible(this.permiso.getNuevo());
		this.btnCancelar.setEnabled(true);
		this.btnEliminar.setEnabled(false);
		this.txtDescripcion.setValue("");
		activarCampos(Boolean.TRUE);
	}

	public void activarCampos(Boolean estado) {
		this.txtDescripcion.setEnabled(estado);

	}

	@AutoGenerated
	private AbsoluteLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new AbsoluteLayout();
		mainLayout.setImmediate(false);
		mainLayout.setWidth("100%");
		mainLayout.setHeight("100%");
		
		// top-level component properties
		setWidth("100.0%");
		setHeight("100.0%");
		
		// tbGrupos
		tbGrupos = new Table();
		tbGrupos.setImmediate(false);
		tbGrupos.setWidth("403px");
		tbGrupos.setHeight("220px");
		mainLayout.addComponent(tbGrupos, "top:40.0px;left:17.0px;");
		
		// lblTitulo
		lblTitulo = new Label();
		lblTitulo.setImmediate(false);
		lblTitulo.setWidth("-1px");
		lblTitulo.setHeight("-1px");
		lblTitulo.setValue("<b>Mantenimiento de Grupos");
		mainLayout.addComponent(lblTitulo, "top:22.0px;left:460.0px;");
		
		// lblDescripcion
		lblDescripcion = new Label();
		lblDescripcion.setImmediate(false);
		lblDescripcion.setWidth("-1px");
		lblDescripcion.setHeight("-1px");
		lblDescripcion.setValue("Descripci&oacute;n: ");
		mainLayout.addComponent(lblDescripcion, "top:60.0px;left:460.0px;");
		
		// txtDescripcion
		txtDescripcion = new TextField();
		txtDescripcion.setImmediate(false);
		txtDescripcion.setWidth("220px");
		txtDescripcion.setHeight("-1px");
		mainLayout.addComponent(txtDescripcion, "top:60.0px;left:540.0px;");
		
		// btnNuevo
		btnNuevo = new Button();
		btnNuevo.setCaption("Nuevo");
		btnNuevo.setIcon(new ThemeResource("images/botones/new.png"));
		btnNuevo.setImmediate(true);
		btnNuevo.setWidth("110px");
		btnNuevo.setHeight("-1px");
		mainLayout.addComponent(btnNuevo, "top:120.0px;left:500.0px;");
		
		// btnGuardar
		btnGuardar = new Button();
		btnGuardar.setCaption("Guardar");
		btnGuardar.setIcon(new ThemeResource("images/botones/save.png"));
		btnGuardar.setImmediate(true);
		btnGuardar.setWidth("110px");
		btnGuardar.setHeight("-1px");
		mainLayout.addComponent(btnGuardar, "top:120.0px;left:640.0px;");
		
		// btnEliminar
		btnEliminar = new Button();
		btnEliminar.setCaption("Eliminar");
		btnEliminar.setIcon(new ThemeResource("images/botones/delete.png"));
		btnEliminar.setImmediate(true);
		btnEliminar.setWidth("110px");
		btnEliminar.setHeight("-1px");
		mainLayout.addComponent(btnEliminar, "top:160.0px;left:500.0px;");
		
		// btnImprimir
		btnImprimir = new Button();
		btnImprimir.setCaption("Imprimir");
		btnImprimir.setIcon(new ThemeResource("images/botones/print.png"));
		btnImprimir.setImmediate(true);
		btnImprimir.setWidth("110px");
		btnImprimir.setHeight("-1px");
		mainLayout.addComponent(btnImprimir, "top:200.0px;left:570.0px;");
		
		// btnCancelar
		btnCancelar = new Button();
		btnCancelar.setCaption("Cancelar");
		btnCancelar.setIcon(new ThemeResource("images/botones/undo.png"));
		btnCancelar.setImmediate(true);
		btnCancelar.setWidth("110px");
		btnCancelar.setHeight("-1px");
		mainLayout.addComponent(btnCancelar, "top:160.0px;left:640.0px;");
		
		// txtDescripcionBuscar
		txtDescripcionBuscar = new TextField();
		txtDescripcionBuscar.setImmediate(false);
		txtDescripcionBuscar.setWidth("360px");
		txtDescripcionBuscar.setHeight("-1px");
		mainLayout
				.addComponent(txtDescripcionBuscar, "top:12.0px;left:60.0px;");
		
		// btnInicio
		btnInicio = new Boton();
		btnInicio.setIcon(new ThemeResource("images/botones/start.png"));
		btnInicio.setImmediate(false);
		btnInicio.setWidth("35px");
		btnInicio.setHeight("-1px");
		mainLayout.addComponent(btnInicio, "top:260.0px;left:105.0px;");
		
		// btnSiguiente
		btnSiguiente = new Boton();
		btnSiguiente.setIcon(new ThemeResource("images/botones/next.png"));
		btnSiguiente.setImmediate(false);
		btnSiguiente.setWidth("35px");
		btnSiguiente.setHeight("-1px");
		mainLayout.addComponent(btnSiguiente, "top:260.0px;left:250.0px;");
		
		// btnFinal
		btnFinal = new Boton();
		btnFinal.setIcon(new ThemeResource("images/botones/final.png"));
		btnFinal.setImmediate(false);
		btnFinal.setWidth("35px");
		btnFinal.setHeight("-1px");
		mainLayout.addComponent(btnFinal, "top:260.0px;left:285.0px;");
		
		// btnAnterior
		btnAnterior = new Boton();
		btnAnterior.setIcon(new ThemeResource("images/botones/previous.png"));
		btnAnterior.setImmediate(false);
		btnAnterior.setWidth("35px");
		btnAnterior.setHeight("-1px");
		mainLayout.addComponent(btnAnterior, "top:260.0px;left:140.0px;");
		
		// txtPaginaActual
		txtPaginaActual = new NumberField();
		txtPaginaActual.setImmediate(false);
		txtPaginaActual.setWidth("25px");
		txtPaginaActual.setHeight("-1px");
		mainLayout.addComponent(txtPaginaActual, "top:265.0px;left:180.0px;");
		
		// label
		label = new Label();
		label.setImmediate(false);
		label.setWidth("-1px");
		label.setHeight("-1px");
		label.setValue("/");
		mainLayout.addComponent(label, "top:265.0px;left:210.0px;");
		
		// lblTotalPaginas
		lblTotalPaginas = new Label();
		lblTotalPaginas.setImmediate(false);
		lblTotalPaginas.setWidth("-1px");
		lblTotalPaginas.setHeight("-1px");
		lblTotalPaginas.setValue("9999");
		mainLayout.addComponent(lblTotalPaginas, "top:265.0px;left:220.0px;");
		
		return mainLayout;
	}

}
