package com.sima.gui;

import javax.servlet.annotation.WebServlet;

import com.sima.gui.PanelPrincipal;
import com.sima.util.SimaUtil;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("sima")
public class SimaUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = SimaUI.class)
	public static class Servlet extends VaadinServlet {
	}
	

	@Override
	protected void init(VaadinRequest request) {
		String usuario=SimaUtil.obtenerUsuarioSesion();
		if(usuario == null){
			PanelLogin panelLogin = new PanelLogin();
			setContent(panelLogin);
		}else{
			cargarPanelPrincipal();
		}
		this.getPage().setTitle("Sistema de Multas Administrativas");
	}
	
	public void cargarPanelPrincipal(){		
		PanelPrincipal panelPrincipal = new PanelPrincipal();
		panelPrincipal.armarMenu();
		panelPrincipal.setWidth("100%");
		panelPrincipal.setHeight("100%");
		setContent(panelPrincipal);
	}

}