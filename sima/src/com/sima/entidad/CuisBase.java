
package com.sima.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sima.util.AuditoriaBean;


@Entity
@Table(name = "cuis_base" , schema="\"NoTributario\"")
public class CuisBase extends AuditoriaBean implements Serializable {
    private static final long serialVersionUID = 1L;
  
   
    private String nombre;
    private String abrev;
   
    @OneToMany(mappedBy = "cuisBase")
    private List<CuisTasa> cuisTasaList;

    public CuisBase() {
    }



    public CuisBase(String nombre, String abrevi) {
        this.nombre = nombre;
        this.abrev = abrevi;
    }

   

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbrev() {
        return abrev;
    }

    public void setAbrev(String abrev) {
        this.abrev = abrev;
    }

    public List<CuisTasa> getCuisTasaList() {
        return cuisTasaList;
    }

    public void setCuisTasaList(List<CuisTasa> cuisTasaList) {
        this.cuisTasaList = cuisTasaList;
    }


    
}
