package com.sima.entidad;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="\"t0008Lugares\"", schema = "public")
public class Lugar implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2474523391496597167L;

	@Id
	@Column(name="c0008idlugar", nullable=false)
	private String id;
	
	
	@Column(name="c0008nombre", nullable=false)
	private String nombre;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="i0008idtiplugar")
	private TipoLugar tipoLugar;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="i0008idsocecon")
	private ClaseSociedadEconomica claseSociedadEconomica;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="i0008idubigeo")
	private ClaseUbigeo claseUbigeo;
	
	@OneToMany(mappedBy="lugar")
	private List<Persona> personas;
	
	//Auditoria
	
		@Column(name="c0008usuario",nullable=false)
		private String usuario;

		@Column(name="b0008estado",nullable=false)
		private Boolean estado;

		@Column(name="d0008fecha",nullable=false)
		private Timestamp fecha;

		@Column(name="c0008pc",nullable=false)
		private String pc;

		@Column(name="c0008ip",nullable=false)
		private String ip;
		
		@Column(name="\"cusuarioMod\"",nullable=false)
		private String usuarioMod;
		
		
		@Column(name="\"cipMod\"",nullable=false)
		private String ipMod;
		
		@Column(name="cobservacion",nullable=true)
		private String observacion;
	
	public Lugar(){
		
	}
	
	



	public String getNombre() {
		return nombre;
	}





	public void setNombre(String nombre) {
		this.nombre = nombre;
	}





	public String getUsuario() {
		return usuario;
	}





	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}





	public Boolean getEstado() {
		return estado;
	}





	public void setEstado(Boolean estado) {
		this.estado = estado;
	}





	public Timestamp getFecha() {
		return fecha;
	}





	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}





	public String getPc() {
		return pc;
	}





	public void setPc(String pc) {
		this.pc = pc;
	}





	public String getIp() {
		return ip;
	}





	public void setIp(String ip) {
		this.ip = ip;
	}





	public String getUsuarioMod() {
		return usuarioMod;
	}





	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}





	public String getIpMod() {
		return ipMod;
	}





	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}





	public String getObservacion() {
		return observacion;
	}





	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}





	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public TipoLugar getTipoLugar() {
		return tipoLugar;
	}



	public void setTipoLugar(TipoLugar tipoLugar) {
		this.tipoLugar = tipoLugar;
		if(!tipoLugar.getLugares().contains(this)){
			tipoLugar.getLugares().add(this);
		}
	}



	public ClaseSociedadEconomica getClaseSociedadEconomica() {
		return claseSociedadEconomica;
	}



	public void setClaseSociedadEconomica(
			ClaseSociedadEconomica claseSociedadEconomica) {
		this.claseSociedadEconomica = claseSociedadEconomica;
		if(!claseSociedadEconomica.getLugares().contains(this)){
			claseSociedadEconomica.getLugares().add(this);
		}
	}



	public ClaseUbigeo getClaseUbigeo() {
		return claseUbigeo;
	}

//On TEST

	public void setClaseUbigeo(ClaseUbigeo claseUbigeo) {
		this.claseUbigeo = claseUbigeo;
		if(!claseUbigeo.getLugares().contains(this)){
			claseUbigeo.addLugar(this);
		}
	}





	public List<Persona> getPersonas() {
		return personas;
	}





	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	
	public void addPersona(Persona persona){
		this.personas.add(persona);
		if(persona.getLugar()!=this){
			persona.setLugar(this);
		}
	}
	
	
	
	
	
	
	
	
}
