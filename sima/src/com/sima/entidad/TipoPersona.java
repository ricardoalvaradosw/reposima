package com.sima.entidad;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="\"t0002TipoPersona\"", schema="public")
public class TipoPersona implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1565682922235319395L;
	
	@Id
	@Column(name="i0002idtipopersona", nullable=false)
	private Integer id;
	
	@Column(name="c0002nombre", nullable=false)
	private String nombre;
	

	//Auditoria
	
	@Column(name="c0006usuario",nullable=false)
	private String usuario;

	@Column(name="b0006estado",nullable=false)
	private Boolean estado;

	@Column(name="d0006fecha",nullable=false)
	private Timestamp fecha;

	@Column(name="c0006pc",nullable=false)
	private String pc;

	@Column(name="c0006ip",nullable=false)
	private String ip;
	
	@Column(name="\"cusuarioMod\"",nullable=false)
	private String usuarioMod;
	
	
	@Column(name="\"cipMod\"",nullable=false)
	private String ipMod;
	
	@Column(name="\"cObservacion\"",nullable=true)
	private String observacion;
	
	//Relations
	@OneToMany(mappedBy="tipoPersona")
	private List<SubTipoPersona> subTipoPersonas;
	
	public TipoPersona(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsuarioMod() {
		return usuarioMod;
	}

	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}

	public String getIpMod() {
		return ipMod;
	}

	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public List<SubTipoPersona> getSubTipoPersonas() {
		return subTipoPersonas;
	}

	public void setSubTipoPersonas(List<SubTipoPersona> subTipoPersonas) {
		this.subTipoPersonas = subTipoPersonas;
	}
	
	public void addSubTipoPersona(SubTipoPersona subTipoPersona){
		this.subTipoPersonas.add(subTipoPersona);
		if(subTipoPersona.getTipoPersona()!=this){
			subTipoPersona.setTipoPersona(this);
		}
	}

}
