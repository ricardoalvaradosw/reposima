package com.sima.entidad;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="\"t0010ClasUbigeo\"", schema="public")
public class ClaseUbigeo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6367204999160800617L;
	@Id
	@Column(name="i0010idubigeo", nullable=false)
	private Integer id;
	@Column(name="c0010nombre", nullable=false)
	private String nombre;
	@Column(name="c0010abrev", nullable=false)
	private String abreviatura;
	
	
	//Auditoria
	
		@Column(name="c0010usuario",nullable=false)
		private String usuario;

		@Column(name="b0010estado",nullable=false)
		private Boolean estado;

		@Column(name="d0010fecha",nullable=false)
		private Timestamp fecha;

		@Column(name="c0010pc",nullable=false)
		private String pc;

		@Column(name="c0010ip",nullable=false)
		private String ip;
		
		@Column(name="\"cusuarioMod\"",nullable=false)
		private String usuarioMod;
		
		
		@Column(name="\"cipMod\"",nullable=false)
		private String ipMod;
		
		@Column(name="cobservacion",nullable=true)
		private String observacion;
	
	
	
	
	
	@OneToMany(mappedBy="")
	private List<Lugar> lugares;





	public Integer getId() {
		return id;
	}





	public void setId(Integer id) {
		this.id = id;
	}





	public String getNombre() {
		return nombre;
	}





	public void setNombre(String nombre) {
		this.nombre = nombre;
	}





	public String getAbreviatura() {
		return abreviatura;
	}





	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}





	public String getUsuario() {
		return usuario;
	}





	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}





	public Boolean getEstado() {
		return estado;
	}





	public void setEstado(Boolean estado) {
		this.estado = estado;
	}





	public Timestamp getFecha() {
		return fecha;
	}





	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}





	public String getPc() {
		return pc;
	}





	public void setPc(String pc) {
		this.pc = pc;
	}





	public String getIp() {
		return ip;
	}





	public void setIp(String ip) {
		this.ip = ip;
	}





	public String getUsuarioMod() {
		return usuarioMod;
	}





	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}





	public String getIpMod() {
		return ipMod;
	}





	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}





	public String getObservacion() {
		return observacion;
	}





	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}





	public List<Lugar> getLugares() {
		return lugares;
	}





	public void setLugares(List<Lugar> lugares) {
		this.lugares = lugares;
	}
	
	public void addLugar(Lugar lugar){
		this.lugares.add(lugar);
		if(lugar.getClaseUbigeo()!=this){
			lugar.setClaseUbigeo(this);
		}
		
	}
	
	

}
