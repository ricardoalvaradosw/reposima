package com.sima.entidad;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="\"t0006Paises\"", schema="public")
public class Pais  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8054553293924882825L;
	@Id
	@Column(name="i0006idpais", nullable=false)
	private Integer idPais;
	@Column(name="c0006nombre", nullable=false)
	private String nombre;
	@Column(name="c0006gentilicio", nullable=false)
	private String gentilicio;
	
	//Auditoria
	
	@Column(name="c0006usuario",nullable=false)
	private String usuario;

	@Column(name="b0006estado",nullable=false)
	private Boolean estado;

	@Column(name="d0006fecha",nullable=false)
	private Timestamp fecha;

	@Column(name="c0006pc",nullable=false)
	private String pc;

	@Column(name="c0006ip",nullable=false)
	private String ip;
	
	@Column(name="\"cusuarioMod\"",nullable=false)
	private String usuarioMod;
	
	
	@Column(name="\"cipMod\"",nullable=false)
	private String ipMod;
	
	@Column(name="\"cObservacion\"",nullable=true)
	private String observacion;
	
	
	//AGREGAR COLECCION PERSONAS
	@OneToMany(mappedBy="pais")
	private List<Persona> personas;
	
	public Pais() {	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGentilicio() {
		return gentilicio;
	}

	public void setGentilicio(String gentilicio) {
		this.gentilicio = gentilicio;
	}

	public Integer getIdPais() {
		return idPais;
	}

	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}
	
	

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsuarioMod() {
		return usuarioMod;
	}

	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}

	public String getIpMod() {
		return ipMod;
	}

	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public void addPersona(Persona persona){
		this.personas.add(persona);
		if(persona.getPais()!=this){
			persona.setPais(this);
		}
	}
	

	
	
	
	
	

}
