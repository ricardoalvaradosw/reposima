
package com.sima.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.sima.util.AuditoriaBean;

@Entity
@Table(name = "clase_vehiculo", schema="\"NoTributario\"")
public class ClaseVehiculo extends AuditoriaBean implements Serializable {
    private static final long serialVersionUID = 1L;
   
   
    private String nombre;
   
    @OneToMany(mappedBy = "claseVehiculo")
    private List<PapeletaAdmVehiculo> papeletaAdmVehiculoList;

    public ClaseVehiculo() {
    }

   

    public ClaseVehiculo(String nombre) {
    	this.nombre = nombre;
    }

   
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<PapeletaAdmVehiculo> getPapeletaAdmVehiculoList() {
        return papeletaAdmVehiculoList;
    }

    public void setPapeletaAdmVehiculoList(List<PapeletaAdmVehiculo> papeletaAdmVehiculoList) {
        this.papeletaAdmVehiculoList = papeletaAdmVehiculoList;
    }

}
