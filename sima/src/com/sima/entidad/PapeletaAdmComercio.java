
package com.sima.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sima.util.AuditoriaBean;

@Entity
@Table(name = "papeleta_adm_comercio", schema="\"NoTributario\"")
public class PapeletaAdmComercio extends AuditoriaBean implements Serializable {
    
    @Column(name = "nombre_comerc")
    private String nombreComerc;
    
    @Column(name = "id_uso")
    private String idUso;
    
    @Column(name = "num_lic_func")
    private String numLicFunc;
    
    @Column(name = "categ")
    private String categ;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "area")
    private BigDecimal area;
    
    @JoinColumn(name = "id_papeleta", referencedColumnName = "id")
    @ManyToOne
    private PapeletaAdm papeletaAdm;

    public PapeletaAdmComercio() {
    }

    public PapeletaAdmComercio(String nombreComerc, String idUso,
			String numLicFunc, String categ, BigDecimal area,
			PapeletaAdm papeletaAdm) {
		super();
		this.nombreComerc = nombreComerc;
		this.idUso = idUso;
		this.numLicFunc = numLicFunc;
		this.categ = categ;
		this.area = area;
		this.papeletaAdm = papeletaAdm;
	}



	public String getNombreComerc() {
        return nombreComerc;
    }

    public void setNombreComerc(String nombreComerc) {
        this.nombreComerc = nombreComerc;
    }

    public String getIdUso() {
        return idUso;
    }

    public void setIdUso(String idUso) {
        this.idUso = idUso;
    }

    public String getNumLicFunc() {
        return numLicFunc;
    }

    public void setNumLicFunc(String numLicFunc) {
        this.numLicFunc = numLicFunc;
    }

    public String getCateg() {
        return categ;
    }

    public void setCateg(String categ) {
        this.categ = categ;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }


    public PapeletaAdm getPapeletaAdm() {
        return papeletaAdm;
    }

    public void setPapeletaAdm(PapeletaAdm papeletaAdm) {
        this.papeletaAdm = papeletaAdm;
    }

    
}
