package com.sima.entidad;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "\"t0013TipoVia\"", schema="public")
public class TipoVia implements Serializable{
	
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "i0013idtipovia")
    private Short id;
    @Basic(optional = false)
    @Column(name = "c0013nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "c0013abrev")
    private String abreviatura;
    
    //Auditoria
    @Basic(optional = false)
    @Column(name = "c0013usuario")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "d0013fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "c0013pc")
    private String pc;
    @Basic(optional = false)
    @Column(name = "c0013ip")
    private String ip;
    @Basic(optional = false)
    @Column(name = "b0013estado")
    private boolean estado;
    @Column(name = "\"cusuarioMod\"")
    private String usuarioMod;
    @Column(name = "cobservacion")
    private String observacion;
    @Column(name = "\"cipMod\"")
    private String ipMod;
    
    //Coleccion Vias
    @OneToMany(cascade=CascadeType.ALL,mappedBy="tipoVia")
    private List<Via> vias;

    public TipoVia() {
    }

    public TipoVia(Short i0013idtipovia) {
        this.id = i0013idtipovia;
    }

    public TipoVia(Short i0013idtipovia, String c0013nombre, String c0013abrev, String c0013usuario, Date d0013fecha, String c0013pc, String c0013ip, boolean b0013estado) {
        this.id = i0013idtipovia;
        this.nombre = c0013nombre;
        this.abreviatura = c0013abrev;
        this.usuario = c0013usuario;
        this.fecha = d0013fecha;
        this.pc = c0013pc;
        this.ip = c0013ip;
        this.estado = b0013estado;
    }

 
    
    public Short getId() {
		return id;
	}

	public void setId(Short id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getUsuarioMod() {
		return usuarioMod;
	}

	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getIpMod() {
		return ipMod;
	}

	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}

	public List<Via> getVias() {
		return vias;
	}

	public void setVias(List<Via> vias) {
		this.vias = vias;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoVia)) {
            return false;
        }
        TipoVia other = (TipoVia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Domain.T0013TipoVia[ i0013idtipovia=" + id + " ]";
    }	
    
    public void addVia(Via via){
    	this.vias.add(via);
    	if(via.getTipoVia()!=this){
    		via.setTipoVia(this);
    	}
    }

}
