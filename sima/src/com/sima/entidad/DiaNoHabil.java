
package com.sima.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sima.util.AuditoriaBean;

@Entity
@Table(name = "diasnohabiles" , schema="\"NoTributario\"")
public class DiaNoHabil extends AuditoriaBean implements Serializable {


    @Column(name = "anio")
    private String anio;
    
    @Column(name = "fecha_no_habil")
    @Temporal(TemporalType.DATE)
    private Date fechaNoHabil;
    
 

    public DiaNoHabil() {
    }

    public DiaNoHabil(String anio, Date fechaNoHabil) {
		this.anio = anio;
		this.fechaNoHabil = fechaNoHabil;
	}


    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public Date getFechaNoHabil() {
        return fechaNoHabil;
    }

    public void setFechaNoHabil(Date fechaNoHabil) {
        this.fechaNoHabil = fechaNoHabil;
    }

  
    
}
