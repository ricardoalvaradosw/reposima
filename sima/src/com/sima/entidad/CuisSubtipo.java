
package com.sima.entidad;



import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sima.util.AuditoriaBean;


@Entity
@Table(name = "cuis_subtipo", schema="\"NoTributario\"")
public class CuisSubtipo extends AuditoriaBean implements Serializable {
 
   
    private String nombre;
    
    @OneToMany(mappedBy = "cuisSubtipo")
    private List<CuisTasa> cuisTasaList;

    public CuisSubtipo() {
    }

 

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<CuisTasa> getCuisTasaList() {
        return cuisTasaList;
    }

    public void setCuisTasaList(List<CuisTasa> cuisTasaList) {
        this.cuisTasaList = cuisTasaList;
    }

 
    
}
