package com.sima.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




@Entity
@Table(name = "\"t0012Vias\"", schema="public")
public class Via implements Serializable{
	
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "c0012codvia")
    private String id;
    
    @Basic(optional = false)
    @Column(name = "c0012nombre")
    private String nombre;
    
    
    //Auditoria
    @Basic(optional = false)
    @Column(name = "c0012usuario")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "d0012fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "c0012pc")
    private String pc;
    @Basic(optional = false)
    @Column(name = "c0012ip")
    private String ip;
    @Basic(optional = false)
    @Column(name = "b0012estado")
    private boolean estado;
    @Column(name = "\"cusuarioMod\"")
    private String usuarioMod;
    @Column(name = "cobservacion")
    private String observacion;
    @Column(name = "\"cipMod\"")
    private String ipMod;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="i0012idtipovia")
    private TipoVia tipoVia;
    
    @OneToMany(mappedBy="via")
    private List<Persona> personas;
  
    public Via() {
    }

    public Via(String c0012codvia) {
        this.id = c0012codvia;
    }

    
  


    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getUsuarioMod() {
		return usuarioMod;
	}

	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getIpMod() {
		return ipMod;
	}

	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Via)) {
            return false;
        }
        Via other = (Via) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Domain.T0012Vias[ c0012codvia=" + id + " ]";
    }

	public TipoVia getTipoVia() {
		return tipoVia;
	}

	public void setTipoVia(TipoVia tipoVia) {
		this.tipoVia = tipoVia;
		if(!tipoVia.getVias().contains(this)){
			tipoVia.getVias().add(this);
		}
	}

	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	
	public void addPersona(Persona persona){
		this.personas.add(persona);
		if(persona.getVia()!=this){
			persona.setVia(this);
		}
	}
    
    
}

