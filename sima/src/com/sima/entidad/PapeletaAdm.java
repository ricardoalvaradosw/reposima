
package com.sima.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sima.util.AuditoriaBean;


@Entity
@Table(name = "papeleta_adm" , schema="\"NoTributario\"")
public class PapeletaAdm extends AuditoriaBean implements Serializable {
    private static final long serialVersionUID = 1L;
  
    @Column(name = "serie")
    private Character serie;
    
    @Column(name = "anio")
    private String anio;
    
    @Column(name = "numero")
    private String numero;
    
    @Column(name = "id_titular")
    private String idTitular;
    
    @Column(name = "id_conductor")
    private String idConductor;
    
    @Column(name = "id_lugar")
    private String idLugar;
    
    @Column(name = "id_via")
    private String idVia;
    
    @Column(name = "num_predio")
    private String numPredio;
    
    @Column(name = "manzana")
    private String manzana;
    
    @Column(name = "lote")
    private String lote;
    
    @Column(name = "fecha_imposicion")
    @Temporal(TemporalType.DATE)
    private Date fechaImposicion;
    
    @Column(name = "hora")
    private String hora;
    
    @Column(name = "id_autoridad")
    private String idAutoridad;
    
    @Column(name = "num_carnet")
    private String numCarnet;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "base_calculo")
    private BigDecimal baseCalculo;
    
    @Column(name = "observaciones")
    private String observaciones;
    
   
    @JoinColumn(name = "id_cuis_tasa", referencedColumnName = "id")
    @ManyToOne
    private CuisTasa cuisTasa;
    
    @OneToMany(mappedBy = "papeletaAdm")
    private List<PapeletaAdmComercio> papeletaAdmComercioList;
    @OneToMany(mappedBy = "papeletaAdm")
    private List<PapeletaAdmVehiculo> papeletaAdmVehiculoList;

    public PapeletaAdm() {
    }

    public PapeletaAdm(Character serie, String anio, String numero,
			String idTitular, String idConductor, String idLugar, String idVia,
			String numPredio, String manzana, String lote,
			Date fechaImposicion, String hora, String idAutoridad,
			String numCarnet, BigDecimal baseCalculo, String observaciones,
			CuisTasa cuisTasa) {
		super();
		this.serie = serie;
		this.anio = anio;
		this.numero = numero;
		this.idTitular = idTitular;
		this.idConductor = idConductor;
		this.idLugar = idLugar;
		this.idVia = idVia;
		this.numPredio = numPredio;
		this.manzana = manzana;
		this.lote = lote;
		this.fechaImposicion = fechaImposicion;
		this.hora = hora;
		this.idAutoridad = idAutoridad;
		this.numCarnet = numCarnet;
		this.baseCalculo = baseCalculo;
		this.observaciones = observaciones;
		this.cuisTasa = cuisTasa;
	}



	public Character getSerie() {
        return serie;
    }

    public void setSerie(Character serie) {
        this.serie = serie;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getIdTitular() {
        return idTitular;
    }

    public void setIdTitular(String idTitular) {
        this.idTitular = idTitular;
    }

    public String getIdConductor() {
        return idConductor;
    }

    public void setIdConductor(String idConductor) {
        this.idConductor = idConductor;
    }

    public String getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(String idLugar) {
        this.idLugar = idLugar;
    }

    public String getIdVia() {
        return idVia;
    }

    public void setIdVia(String idVia) {
        this.idVia = idVia;
    }

    public String getNumPredio() {
        return numPredio;
    }

    public void setNumPredio(String numPredio) {
        this.numPredio = numPredio;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Date getFechaImposicion() {
        return fechaImposicion;
    }

    public void setFechaImposicion(Date fechaImposicion) {
        this.fechaImposicion = fechaImposicion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIdAutoridad() {
        return idAutoridad;
    }

    public void setIdAutoridad(String idAutoridad) {
        this.idAutoridad = idAutoridad;
    }

    public String getNumCarnet() {
        return numCarnet;
    }

    public void setNumCarnet(String numCarnet) {
        this.numCarnet = numCarnet;
    }

    public BigDecimal getBaseCalculo() {
        return baseCalculo;
    }

    public void setBaseCalculo(BigDecimal baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    

    public CuisTasa getCuisTasa() {
        return cuisTasa;
    }

    public void setCuisTasa(CuisTasa cuisTasa) {
        this.cuisTasa = cuisTasa;
    }

    public List<PapeletaAdmComercio> getPapeletaAdmComercioList() {
        return papeletaAdmComercioList;
    }

    public void setPapeletaAdmComercioList(List<PapeletaAdmComercio> papeletaAdmComercioList) {
        this.papeletaAdmComercioList = papeletaAdmComercioList;
    }

    public List<PapeletaAdmVehiculo> getPapeletaAdmVehiculoList() {
        return papeletaAdmVehiculoList;
    }

    public void setPapeletaAdmVehiculoList(List<PapeletaAdmVehiculo> papeletaAdmVehiculoList) {
        this.papeletaAdmVehiculoList = papeletaAdmVehiculoList;
    }

    
}
