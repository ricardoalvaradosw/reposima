
package com.sima.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.sima.util.AuditoriaBean;


@Entity
@Table(name = "cuis_tasa", schema="\"NoTributario\"")
public class CuisTasa extends AuditoriaBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "porcentaje")
    private BigDecimal porcentaje;
    
   
    @OneToMany(mappedBy = "cuisTasa")
    private List<PapeletaAdm> papeletaAdmList;
    
    @JoinColumn(name = "id_subtipo", referencedColumnName = "id")
    @ManyToOne
    private CuisSubtipo cuisSubtipo;
    
    @JoinColumn(name = "id_cuis_detalle", referencedColumnName = "id")
    @ManyToOne
    private CuisDetalle cuisDetalle;
    
    @JoinColumn(name = "id_base", referencedColumnName = "id")
    @ManyToOne
    private CuisBase cuisBase;
    
    @Transient
    private Long nroItem;

    public CuisTasa() {
    }

    public CuisTasa(BigDecimal porcentaje, CuisSubtipo cuisSubtipo,
			CuisDetalle cuisDetalle, CuisBase cuisBase) {
		super();
		this.porcentaje = porcentaje;
		this.cuisSubtipo = cuisSubtipo;
		this.cuisDetalle = cuisDetalle;
		this.cuisBase = cuisBase;
	}




	public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }


    public List<PapeletaAdm> getPapeletaAdmList() {
        return papeletaAdmList;
    }

    public void setPapeletaAdmList(List<PapeletaAdm> papeletaAdmList) {
        this.papeletaAdmList = papeletaAdmList;
    }

    public CuisSubtipo getCuisSubtipo() {
        return cuisSubtipo;
    }

    public void setCuisSubtipo(CuisSubtipo cuisSubtipo) {
        this.cuisSubtipo = cuisSubtipo;
    }

    public CuisDetalle getCuisDetalle() {
        return cuisDetalle;
    }

    public void setCuisDetalle(CuisDetalle cuisDetalle) {
        this.cuisDetalle = cuisDetalle;
    }

    public CuisBase getCuisBase() {
        return cuisBase;
    }

    public void setCuisBase(CuisBase cuisBase) {
        this.cuisBase = cuisBase;
    }

	public Long getNroItem() {
		return nroItem;
	}

	public void setNroItem(Long nroItem) {
		this.nroItem = nroItem;
	}
    
    

 
    
}
