package com.sima.entidad;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="\"t0003SubTippersona\"", schema="public")
public class SubTipoPersona implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8249388580501387238L;

	@Id
	@Column(name="i0003idsubtippers", nullable=false)
	private Integer id;
	
	@Column(name="c0003nombre", nullable=false)
	private String nombre;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="i0003idtipopersona")
	private TipoPersona  tipoPersona;
	
	//Coleccion de Personas
	@OneToMany(mappedBy="subTipoPersona")
	private List<Persona> personas;
	
	//Auditoria
	
		@Column(name="c0003usuario",nullable=false)
		private String usuario;

		@Column(name="b0003estado",nullable=false)
		private Boolean estado;

		@Column(name="d0003fecha",nullable=false)
		private Timestamp fecha;

		@Column(name="c0003pc",nullable=false)
		private String pc;

		@Column(name="c0003ip",nullable=false)
		private String ip;
		
		@Column(name="\"cusuarioMod\"",nullable=false)
		private String usuarioMod;
		
		
		@Column(name="\"cipMod\"",nullable=false)
		private String ipMod;
		
		@Column(name="cobservacion",nullable=true)
		private String observacion;
	
	public SubTipoPersona(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsuarioMod() {
		return usuarioMod;
	}

	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}

	public String getIpMod() {
		return ipMod;
	}

	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	
	public void setPais(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
		if(!tipoPersona.getSubTipoPersonas().contains(this)){
			tipoPersona.getSubTipoPersonas().add(this);
		}
	}

	
	
	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public void addPersona(Persona persona){
		this.personas.add(persona);
		if(persona.getSubTipoPersona()!=this){
			persona.setSubTipoPersona(this);
		}
	}
	

}
