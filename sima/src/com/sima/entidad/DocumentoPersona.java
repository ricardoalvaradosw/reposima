package com.sima.entidad;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="\"t0016PersonasDocIden\"", schema="public")
public class DocumentoPersona implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5767874422177398157L;

	@EmbeddedId
	private DocumentoPersonaId id;
	
	
	@Column(name="c0016numero", nullable=false)
	private String numero;
	
	
	//Auditoria
	
			@Column(name="c0016usuario",nullable=false)
			private String usuario;

			@Column(name="b0016estado",nullable=false)
			private Boolean estado;

			@Column(name="d0016fecha",nullable=false)
			private Timestamp fecha;

			@Column(name="c0016pc",nullable=false)
			private String pc;

			@Column(name="c0016ip",nullable=false)
			private String ip;
			
			@Column(name="\"cusuarioMod\"",nullable=false)
			private String usuarioMod;
			
			
			@Column(name="\"cipMod\"",nullable=false)
			private String ipMod;
			
			@Column(name="cobservacion",nullable=true)
			private String observacion;
			
			
		    @JoinColumn(name = "c0016codpersona", insertable=false, updatable=false)
		    @ManyToOne(optional = false, fetch = FetchType.LAZY)
		    private Persona persona;
		
		public DocumentoPersona(){
			
		}

	

		public DocumentoPersonaId getId() {
			return id;
		}



		public void setId(DocumentoPersonaId id) {
			this.id = id;
		}



		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		public String getUsuario() {
			return usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}

		public Boolean getEstado() {
			return estado;
		}

		public void setEstado(Boolean estado) {
			this.estado = estado;
		}

		public Timestamp getFecha() {
			return fecha;
		}

		public void setFecha(Timestamp fecha) {
			this.fecha = fecha;
		}

		public String getPc() {
			return pc;
		}

		public void setPc(String pc) {
			this.pc = pc;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getUsuarioMod() {
			return usuarioMod;
		}

		public void setUsuarioMod(String usuarioMod) {
			this.usuarioMod = usuarioMod;
		}

		public String getIpMod() {
			return ipMod;
		}

		public void setIpMod(String ipMod) {
			this.ipMod = ipMod;
		}

		public String getObservacion() {
			return observacion;
		}

		public void setObservacion(String observacion) {
			this.observacion = observacion;
		}

		public Persona getPersona() {
			return persona;
		}

		public void setPersona(Persona persona) {
			this.persona = persona;
			if(!persona.getDocumentosIdentidad().contains(this)){
				persona.getDocumentosIdentidad().add(this);
			}
		}



		
		
		
	
}
