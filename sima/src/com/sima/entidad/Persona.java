package com.sima.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




@Entity
@Table(name="\"t0001Personas\"", schema="public")
public class Persona implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6002534636665686361L;

	@Id
	@Column(name="c0001codpersona", nullable=false)
	private String idPersona;
	
	@Column(name="b0001contribuyente", nullable=false)
	private Boolean contribuyente;
	
	@Column(name="c0001nombre", nullable=false)
	private String nombre;
	
	@Column(name="c0001numero", nullable=false)
	private String numero;
	@Column(name="c0001dpto", nullable=false)
	private String departamento;
	@Column(name="c0001piso", nullable=false)
	private String piso;
	@Column(name="c0001block", nullable=false)
	private String bloque;
	@Column(name="c0001manzana", nullable=false)
	private String manzana;
	@Column(name="c0001lote", nullable=false)
	private String lote;
	 //Auditoria
    @Basic()
    @Column(name = "c0001usuario")
    private String usuario;
    @Basic()
    @Column(name = "d0001fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic()
    @Column(name = "c0001pc")
    private String pc;
    @Basic()
    @Column(name = "c0001ip")
    private String ip;
    @Basic()
    @Column(name = "b0001estado")
    private boolean estado;
    @Column(name = "\"cusuarioMod\"")
    private String usuarioMod;
    @Column(name = "cobservacion")
    private String observacion;
    @Column(name = "\"cipMod\"")
    private String ipMod;
    
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="i0001idpais")
	private Pais pais;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="i0001subtippers")
	private SubTipoPersona subTipoPersona;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="c0001idlugar")
	private Lugar lugar;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="c0001codvia")
	private Via via;
	
	@OneToMany(mappedBy = "persona",fetch=FetchType.LAZY)
	private List<DocumentoPersona> documentosIdentidad;
	

	public Persona(){
		
	}
	
	

	public Persona(String idPersona, Boolean contribuyente, String nombre,
			String numero, String departamento, String piso, String bloque,
			String manzana, String lote, String usuario, Date fecha, String pc,
			String ip, boolean estado, String usuarioMod, String observacion,
			String ipMod, Pais pais, SubTipoPersona subTipoPersona,
			Lugar lugar, Via via, List<DocumentoPersona> documentosIdentidad) {
		super();
		this.idPersona = idPersona;
		this.contribuyente = contribuyente;
		this.nombre = nombre;
		this.numero = numero;
		this.departamento = departamento;
		this.piso = piso;
		this.bloque = bloque;
		this.manzana = manzana;
		this.lote = lote;
		this.usuario = usuario;
		this.fecha = fecha;
		this.pc = pc;
		this.ip = ip;
		this.estado = estado;
		this.usuarioMod = usuarioMod;
		this.observacion = observacion;
		this.ipMod = ipMod;
		this.pais = pais;
		this.subTipoPersona = subTipoPersona;
		this.lugar = lugar;
		this.via = via;
		this.documentosIdentidad = documentosIdentidad;
	}



	public String getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(String idPersona) {
		this.idPersona = idPersona;
	}

	public Pais getPais() {
		return pais;
	}
	
	
	
	

	public Boolean getContribuyente() {
		return contribuyente;
	}

	public void setContribuyente(Boolean contribuyente) {
		this.contribuyente = contribuyente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getBloque() {
		return bloque;
	}

	public void setBloque(String bloque) {
		this.bloque = bloque;
	}

	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}
	
	
	

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getUsuarioMod() {
		return usuarioMod;
	}

	public void setUsuarioMod(String usuarioMod) {
		this.usuarioMod = usuarioMod;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getIpMod() {
		return ipMod;
	}

	public void setIpMod(String ipMod) {
		this.ipMod = ipMod;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
		if(!pais.getPersonas().contains(this)){
			pais.getPersonas().add(this);
		}
	}

	public SubTipoPersona getSubTipoPersona() {
		return subTipoPersona;
	}

	public void setSubTipoPersona(SubTipoPersona subTipoPersona) {
		this.subTipoPersona = subTipoPersona;
		if(!subTipoPersona.getPersonas().contains(this)){
			subTipoPersona.getPersonas().add(this);
		}
	}

	public Lugar getLugar() {
		return lugar;
	}

	public void setLugar(Lugar lugar) {
		this.lugar = lugar;
		if(!lugar.getPersonas().contains(this)){
			lugar.getPersonas().add(this);
		}
	}

	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
		if(!via.getPersonas().contains(this)){
			via.getPersonas().add(this);
		}
	}

	public List<DocumentoPersona> getDocumentosIdentidad() {
		return documentosIdentidad;
	}

	public void setDocumentosIdentidad(List<DocumentoPersona> documentosIdentidad) {
		this.documentosIdentidad = documentosIdentidad;
	}
	
	
	
	
	
	
	
	
	

}