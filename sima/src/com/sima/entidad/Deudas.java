
package com.sima.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sima.util.AuditoriaBean;


@Entity
@Table(name = "deudas", schema="\"NoTributario\"")
public class Deudas extends AuditoriaBean implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "cod_infractor")
    private String codInfractor;
    
    @Column(name = "serie")
    private Character serie;
    
    @Column(name = "anio")
    private String anio;
    
    @Column(name = "numero")
    private String numero;
    
    @Column(name = "cod_trib")
    private String codTrib;
    
    @Column(name = "cod_fase")
    private String codFase;
    
    @Column(name = "cod_concepto")
    private String codConcepto;
    
    @Column(name = "periodo")
    private String periodo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cargo_importe")
    private BigDecimal cargoImporte;
    
    @Column(name = "abono_importe")
    private BigDecimal abonoImporte;
    
    @Column(name = "alta_importe")
    private BigDecimal altaImporte;
    
    @Column(name = "baja_importe")
    private BigDecimal bajaImporte;
    
    @Column(name = "cargo_reajuste")
    private BigDecimal cargoReajuste;
    
    @Column(name = "abono_reajuste")
    private BigDecimal abonoReajuste;
    
    @Column(name = "alta_reajuste")
    private BigDecimal altaReajuste;
    
    @Column(name = "baja_reajuste")
    private BigDecimal bajaReajuste;
    
    @Column(name = "cargo_gasto")
    private BigDecimal cargoGasto;
    
    @Column(name = "abono_gasto")
    private BigDecimal abonoGasto;
    
    @Column(name = "alta_gasto")
    private BigDecimal altaGasto;
    
    @Column(name = "baja_gasto")
    private BigDecimal bajaGasto;
    
    @Column(name = "fecha_imposicion")
    @Temporal(TemporalType.DATE)
    private Date fechaImposicion;
    
    @Column(name = "fecha_carga")
    @Temporal(TemporalType.DATE)
    private Date fechaCarga;
    
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.DATE)
    private Date fechaPago;
    
    @Column(name = "resp_carga")
    private String respCarga;
    
    @Column(name = "estado_deu")
    private Character estadoDeu;
  

    public Deudas() {
    }

    public Deudas(String codInfractor, Character serie, String anio,
			String numero, String codTrib, String codFase, String codConcepto,
			String periodo, BigDecimal cargoImporte, BigDecimal abonoImporte,
			BigDecimal altaImporte, BigDecimal bajaImporte,
			BigDecimal cargoReajuste, BigDecimal abonoReajuste,
			BigDecimal altaReajuste, BigDecimal bajaReajuste,
			BigDecimal cargoGasto, BigDecimal abonoGasto, BigDecimal altaGasto,
			BigDecimal bajaGasto, Date fechaImposicion, Date fechaCarga,
			Date fechaPago, String respCarga, Character estadoDeu) {
		super();
		this.codInfractor = codInfractor;
		this.serie = serie;
		this.anio = anio;
		this.numero = numero;
		this.codTrib = codTrib;
		this.codFase = codFase;
		this.codConcepto = codConcepto;
		this.periodo = periodo;
		this.cargoImporte = cargoImporte;
		this.abonoImporte = abonoImporte;
		this.altaImporte = altaImporte;
		this.bajaImporte = bajaImporte;
		this.cargoReajuste = cargoReajuste;
		this.abonoReajuste = abonoReajuste;
		this.altaReajuste = altaReajuste;
		this.bajaReajuste = bajaReajuste;
		this.cargoGasto = cargoGasto;
		this.abonoGasto = abonoGasto;
		this.altaGasto = altaGasto;
		this.bajaGasto = bajaGasto;
		this.fechaImposicion = fechaImposicion;
		this.fechaCarga = fechaCarga;
		this.fechaPago = fechaPago;
		this.respCarga = respCarga;
		this.estadoDeu = estadoDeu;
	}



    public String getCodInfractor() {
        return codInfractor;
    }

    public void setCodInfractor(String codInfractor) {
        this.codInfractor = codInfractor;
    }

    public Character getSerie() {
        return serie;
    }

    public void setSerie(Character serie) {
        this.serie = serie;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodTrib() {
        return codTrib;
    }

    public void setCodTrib(String codTrib) {
        this.codTrib = codTrib;
    }

    public String getCodFase() {
        return codFase;
    }

    public void setCodFase(String codFase) {
        this.codFase = codFase;
    }

    public String getCodConcepto() {
        return codConcepto;
    }

    public void setCodConcepto(String codConcepto) {
        this.codConcepto = codConcepto;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public BigDecimal getCargoImporte() {
        return cargoImporte;
    }

    public void setCargoImporte(BigDecimal cargoImporte) {
        this.cargoImporte = cargoImporte;
    }

    public BigDecimal getAbonoImporte() {
        return abonoImporte;
    }

    public void setAbonoImporte(BigDecimal abonoImporte) {
        this.abonoImporte = abonoImporte;
    }

    public BigDecimal getAltaImporte() {
        return altaImporte;
    }

    public void setAltaImporte(BigDecimal altaImporte) {
        this.altaImporte = altaImporte;
    }

    public BigDecimal getBajaImporte() {
        return bajaImporte;
    }

    public void setBajaImporte(BigDecimal bajaImporte) {
        this.bajaImporte = bajaImporte;
    }

    public BigDecimal getCargoReajuste() {
        return cargoReajuste;
    }

    public void setCargoReajuste(BigDecimal cargoReajuste) {
        this.cargoReajuste = cargoReajuste;
    }

    public BigDecimal getAbonoReajuste() {
        return abonoReajuste;
    }

    public void setAbonoReajuste(BigDecimal abonoReajuste) {
        this.abonoReajuste = abonoReajuste;
    }

    public BigDecimal getAltaReajuste() {
        return altaReajuste;
    }

    public void setAltaReajuste(BigDecimal altaReajuste) {
        this.altaReajuste = altaReajuste;
    }

    public BigDecimal getBajaReajuste() {
        return bajaReajuste;
    }

    public void setBajaReajuste(BigDecimal bajaReajuste) {
        this.bajaReajuste = bajaReajuste;
    }

    public BigDecimal getCargoGasto() {
        return cargoGasto;
    }

    public void setCargoGasto(BigDecimal cargoGasto) {
        this.cargoGasto = cargoGasto;
    }

    public BigDecimal getAbonoGasto() {
        return abonoGasto;
    }

    public void setAbonoGasto(BigDecimal abonoGasto) {
        this.abonoGasto = abonoGasto;
    }

    public BigDecimal getAltaGasto() {
        return altaGasto;
    }

    public void setAltaGasto(BigDecimal altaGasto) {
        this.altaGasto = altaGasto;
    }

    public BigDecimal getBajaGasto() {
        return bajaGasto;
    }

    public void setBajaGasto(BigDecimal bajaGasto) {
        this.bajaGasto = bajaGasto;
    }

    public Date getFechaImposicion() {
        return fechaImposicion;
    }

    public void setFechaImposicion(Date fechaImposicion) {
        this.fechaImposicion = fechaImposicion;
    }

    public Date getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getRespCarga() {
        return respCarga;
    }

    public void setRespCarga(String respCarga) {
        this.respCarga = respCarga;
    }

    public Character getEstadoDeu() {
        return estadoDeu;
    }

    public void setEstadoDeu(Character estadoDeu) {
        this.estadoDeu = estadoDeu;
    }

 
    
}
