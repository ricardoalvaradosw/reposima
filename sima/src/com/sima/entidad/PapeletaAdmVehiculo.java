
package com.sima.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sima.util.AuditoriaBean;

@Entity
@Table(name = "papeleta_adm_vehiculo", schema="\"NoTributario\"")
public class PapeletaAdmVehiculo extends AuditoriaBean implements Serializable {
  
    @Column(name = "placa")
    private String placa;
    
    @Column(name = "color")
    private String color;
    
    @Column(name = "marca")
    private String marca;
   
    @JoinColumn(name = "id_papeleta", referencedColumnName = "id")
    @ManyToOne
    private PapeletaAdm papeletaAdm;
    
    @JoinColumn(name = "id_clase_veh", referencedColumnName = "id")
    @ManyToOne
    private ClaseVehiculo claseVehiculo;

    public PapeletaAdmVehiculo() {
    }


    public PapeletaAdmVehiculo(String placa, String color, String marca,
			PapeletaAdm papeletaAdm, ClaseVehiculo claseVehiculo) {
		super();
		this.placa = placa;
		this.color = color;
		this.marca = marca;
		this.papeletaAdm = papeletaAdm;
		this.claseVehiculo = claseVehiculo;
	}


	public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

  
    public PapeletaAdm getPapeletaAdm() {
        return papeletaAdm;
    }

    public void setPapeletaAdm(PapeletaAdm papeletaAdm) {
        this.papeletaAdm = papeletaAdm;
    }

    public ClaseVehiculo getClaseVehiculo() {
        return claseVehiculo;
    }

    public void setClaseVehiculo(ClaseVehiculo claseVehiculo) {
        this.claseVehiculo = claseVehiculo;
    }

}
