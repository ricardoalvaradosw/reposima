
package com.sima.entidad;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.sima.entidad.*;
import com.sima.util.AuditoriaBean;


@Entity
@Table(name = "cuis_detalle", schema="\"NoTributario\"")
public class CuisDetalle extends AuditoriaBean implements Serializable {
    private static final long serialVersionUID = 1L;
   
   
    private String codigo;
   
    private String nombre;
    
    private String anio;
    
    @Column(name = "sancion_complem")
    private String sancionComplem;
    
    @OneToMany(mappedBy = "cuisDetalle")
    private List<CuisTasa> cuisTasaList;

    public CuisDetalle() {
    }
 
    public CuisDetalle(String codigo, String nombre,String anio, String sancionComplem) {
		
		this.codigo = codigo;
		this.nombre = nombre;
		this.anio = anio;
		this.sancionComplem = sancionComplem;
	}



	public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getSancionComplem() {
        return sancionComplem;
    }

    public void setSancionComplem(String sancionComplem) {
        this.sancionComplem = sancionComplem;
    }

 
    public List<CuisTasa> getCuisTasaList() {
        return cuisTasaList;
    }

    public void setCuisTasaList(List<CuisTasa> cuisTasaList) {
        this.cuisTasaList = cuisTasaList;
    }

    
}
