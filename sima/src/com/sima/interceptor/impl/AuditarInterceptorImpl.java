package com.sima.interceptor.impl;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.stereotype.Component;

import com.sima.interceptor.AuditarInterceptor;
import com.sima.util.AuditoriaBean;
import com.sima.util.SimaUtil;


@Component("auditarInterceptor")
public class AuditarInterceptorImpl implements AuditarInterceptor, Serializable{
	
	private static final long serialVersionUID = -2269463476088223892L;	
	private final static String METODO_CREAR = "crear";
	private final static String METODO_ACTUALIZAR = "actualizar";
	private final static String METODO_GRABAR_TODOS = "grabarTodos";

	@SuppressWarnings({ "rawtypes" })	
	public Object invoke(MethodInvocation invocation) throws Throwable{		
	
		if (validarInvocacion(invocation)){
//			String usuario = SimaUtil.obtenerUsuarioSesion();
			String usuario ="admin";
			Object parametro = invocation.getArguments()[0];
			Date date=new Date();
			Timestamp fechaHora = new Timestamp(date.getTime());			
			if (METODO_CREAR.equals(invocation.getMethod().getName())
					|| METODO_ACTUALIZAR.equals(invocation.getMethod().getName())){
				AuditoriaBean auditoriaBean = ((AuditoriaBean)parametro);
				if(auditoriaBean.esNuevo()){
					auditoriaBean.setCreadoPor(usuario);
					auditoriaBean.setFechaCreacion(fechaHora);
					auditoriaBean.setCreadoPorip(SimaUtil.obtenerIpUsuairo());
					auditoriaBean.setCreadoPorhostName(SimaUtil.obtenerHostNameUsuario());
					auditoriaBean.setEstado(Boolean.TRUE);
				}	
				auditoriaBean.setModificadoPor(usuario);
				auditoriaBean.setFechaModificacion(fechaHora);
				auditoriaBean.setModificadoPorip(SimaUtil.obtenerIpUsuairo());
				auditoriaBean.setModificadoPorhostName(SimaUtil.obtenerHostNameUsuario());
			}
			if (METODO_GRABAR_TODOS.equals(invocation.getMethod().getName())){
				List auditoriaBeans = (List) parametro;
				for (Object obj : auditoriaBeans) {
					AuditoriaBean auditoriaBean;
					if(obj instanceof AuditoriaBean){
						auditoriaBean = (AuditoriaBean) obj; 
					}else{
						continue;
					}					
					if(auditoriaBean.esNuevo()){
						auditoriaBean.setCreadoPor(usuario);
						auditoriaBean.setFechaCreacion(fechaHora);
						auditoriaBean.setEstado(Boolean.TRUE);
						auditoriaBean.setCreadoPorip(SimaUtil.obtenerIpUsuairo());
						auditoriaBean.setCreadoPorhostName(SimaUtil.obtenerHostNameUsuario());
						
					}
						auditoriaBean.setModificadoPor(usuario);
						auditoriaBean.setFechaModificacion(fechaHora);
						auditoriaBean.setModificadoPorip(SimaUtil.obtenerIpUsuairo());
						auditoriaBean.setModificadoPorhostName(SimaUtil.obtenerHostNameUsuario());
				}
			}
		}
		return invocation.proceed();
	}

	private boolean validarInvocacion(MethodInvocation invocation) {
		if (invocation.getArguments().length == 1){
			Object parametro = invocation.getArguments()[0];
			return (parametro instanceof AuditoriaBean || 
							parametro instanceof List<?>) &&
							METODO_CREAR.equals(invocation.getMethod().getName()) ||
							METODO_ACTUALIZAR.equals(invocation.getMethod().getName()) ||
							METODO_GRABAR_TODOS.equals(invocation.getMethod().getName());
		}
		return false;
	}
}